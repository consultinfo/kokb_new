<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Пациентам");
$APPLICATION->SetPageProperty("description", "Пациентам");
$APPLICATION->SetTitle("Пациентам");
?><p class="MsoNormal" align="center" style="text-align: center; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><b><span style="line-height: 150%;">ПРАВИЛА</span></b><span style="line-height: 150%;"> </span></span>
</p>
<p class="MsoNormal" align="center" style="text-align: center; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><b><span style="line-height: 150%;">приема пациентов в консультативно-диагностической поликлинике Областной клинической больницы Калининградской области по программе ОМС (2016 &nbsp;год)</span></b></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">1.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">В консультативно-диагностической поликлинике областной клинической больницы Калининградской области принимаются пациенты, направленные специалистами государственных лечебных учреждений области. Без направления (временно) разрешается осуществлять запись пациентов к специалистам онкологического подразделения ОКБ.</span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">2.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Пациент при обращении в консультативно-диагностическую поликлинику должен иметь при себе: </span></span>
</p>
<p class="MsoNormal" style="margin-left: 49.65pt; text-indent: 0cm; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">o<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Направление установленного образца, заверенное подписью руководителя государственного ЛПУ и печатью соответствующего ЛПУ с указанием номера кабинета (специалиста), даты и времени приема;</span></span>
</p>
<p class="MsoNormal" style="margin-left: 49.65pt; text-indent: 0cm; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">o<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Медицинскую карту амбулаторного больного или выписку из нее с данными проведенного лечения и обследования;</span></span>
</p>
<p class="MsoNormal" style="margin-left: 49.65pt; text-indent: 0cm; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">o<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><b><u><span style="line-height: 150%;">Страховой медицинский полис (действующий);</span></u></b></span>
</p>
<p class="MsoNormal" style="margin-left: 49.65pt; text-indent: 0cm; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">o<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Документ, удостоверяющий личность; <br>
	 (Данные требования относятся и к направленным на обследование призывникам). </span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">3.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Все консультативные приемы пациентов осуществляются в соответствии с действующим<b> «Расписанием консультативных приемов»</b> по предварительной записи через медицинскую информационную систему "БАРС".&nbsp;</span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">4.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Пациенты, прибывшие на прием с опозданием, принимаются врачом в конце приема, либо прием для них переносится на другую дату. </span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">5.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Льготные категории граждан пользуются правом внеочередного приема согласно действующему законодательству. К этой же категории отнесены и медицинские работники. </span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">6.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Результаты консультаций оформляются в виде заключения специалиста и выдаются больному для передачи лечащему врачу. </span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">7.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Прием больных, имеющих полис добровольного медицинского страхования, осуществляется в специально выделенное время.</span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
 <span style="font-family: &quot;Times New Roman&quot;; font-size: 16px; color: #000000;"><span style="line-height: 150%;">8.<span style="font-variant-numeric: normal; font-stretch: normal; line-height: normal;">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="line-height: 150%;">Спорные и неясные вопросы, возникшие во время пребывания в поликлинике, Вы можете разрешить:</span></span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">В регистратуре консультативно-диагностической поликлиники, &nbsp;телефоны: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">578-575, 578-699</b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.&nbsp;</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">В регистратуре онкологического отделения телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">533-976</b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У старшей медицинской сестры &nbsp;Дешкович Натальи Валентиновны, каб. 211, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">578-577</b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У старшей медицинской сестры онкологического подразделения поликлиники Голотиной Светланы Анатольевны, каб.310 , телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">578-904</b>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У руководителя онкологической службы &nbsp;Попова Михаила Серафимовича, каб. 413, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">592-912</b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У заместителя главного врача по поликлиническому разделу работы Саврасовой Татьяны Александровны, каб. № 209, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">578-556</b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У заместителя главного врача по лечебной работе Вайсбейна Игоря Зиновьевича, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt;"><span style="color: #000000;">578-555</span></b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У заместителя главного врача по терапии Кобера Дениса Владимировича, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt;"><span style="color: #000000;">578- 457</span></b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
<p class="MsoNormal" style="margin-left: 36pt; text-indent: -18pt; line-height: 150%;">
	<span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">У главного врача ОКБ Вискова Романа Владимировича, телефон: </span><b style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt;"><span style="color: #000000;">578-558</span></b><span style="font-family: &quot;Times New Roman&quot;, Times; font-size: 12pt; color: #000000;">.</span>
</p>
 <br>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>