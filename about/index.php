<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О клинике");
?>
    <img src="/upload/content/main_text.jpg" style="float:left;margin:0 10px 5px 0"  />
    <p><b>Государственное бюджетное учреждение здравоохранения &laquo;Областная клиническая больница Калининградской области&raquo;</b>
        оказывает специализированную и высокотехнологичную медицинскую помощь населению области. В больнице работают 285 врачей
        более 30-ти медицинских специальностей. 147 врачей имеют высшую квалификационную категорию, в коллективе 24 кандидата и 2
        доктора медицинских наук, 19-ти врачам присвоено звание &laquo;Заслуженный врач Российской Федерации&raquo;. Больница является одним из
        старейших медицинских учреждений региона &mdash; ровесником Калининградской области. Год ее рождения &mdash; 1946-й. За шесть с половиной
        десятилетий в коллективе сложились прочные традиции. В то же время областная клиническая больница постоянно развивается и
        идет вперед. Современные медицинские технологии и методики лечения, высокий профессионализм врачей &mdash; вот сегодняшние визитные
        карточки Калининградской ОКБ. А наш девиз остается прежним: &laquo;Стабильность и милосердие&raquo;. В последние годы произошли серьезные
        положительные изменения в материально-техническом и бытовом оснащении больницы. Идет активное обновление лечебного и
        диагностического оборудования по Программе модернизации здравоохранения. Все это дает возможность оказывать в больнице
        многопрофильную медицинскую помощь на высоком современном уровне.
    </p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

