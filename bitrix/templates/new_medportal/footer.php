<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();?>

    <?if(empty($white_block)):?>
    </div>
    <?endif;?>

</div>
</main>
<footer class="footer">
    <div class="footer__navigation">
        <?$APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top_menu",
            array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "1",
                "MENU_CACHE_GET_VARS" => array(
                ),
                "MENU_CACHE_TIME" => "3600",
                "MENU_CACHE_TYPE" => "N",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "COMPONENT_TEMPLATE" => "top_menu"
            ),
            false
        );?>
    </div>
    <div class="container">
        <div class="footer__advantage">
            <h2 class="visually-hidden">Преимущества</h2>
            <ul class="advantage__list">
                <li class="advantage__item">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/link_infooter1.php", Array(), Array("MODE" => "html", "NAME" =>"ссылку"));?>
                </li>
                <li class="advantage__item">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/link_infooter2.php", Array(), Array("MODE" => "html", "NAME" =>"ссылку"));?>
                </li>
                <li class="advantage__item">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/link_infooter3.php", Array(), Array("MODE" => "html", "NAME" =>"ссылку"));?>
                </li>
                <li class="advantage__item">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/link_infooter4.php", Array(), Array("MODE" => "html", "NAME" =>"ссылку"));?>
                </li>
                <li class="advantage__item">
                    <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/link_infooter5.php", Array(), Array("MODE" => "html", "NAME" =>"ссылку"));?>
                </li>
            </ul>
        </div>
        <div class="copyright">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/copyright.php", Array(), Array("MODE" => "html", "NAME" =>"ссылки"));?>
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/footer/consult-info.php", Array(), Array("MODE" => "html", "NAME" =>"ссылки"));?>
        </div>
    </div>
</footer>

</body>
</html>
</body>
</html>