$(document).on('ready',function(){
    $(window).scroll(function(){

        if ($(this).scrollTop() > 150) {
            $('.header__navigation').addClass('header--fixed');

        }else{
            $('.header__navigation').removeClass('header--fixed');
        }
    });

    slider();
    mobile_nav();

//    if(window.innerWidth >= 992){
//        document.getElementById('video-container').innerHTML = '<video class="video-bg" width="1920" height="500" poster="templates/consultinfo/images/video.jpg" autoplay loop><source src="templates/consultinfo/video/clinic_1.mp4" type="video/mp4;"><source src="templates/consultinfo/video/clinic_1_1.webm" type="video/webm"></video>'
//    }

});
$(window).on('resize',function(){
    $(window).scroll(function(){

        if ($(this).scrollTop() > 150) {
            $('.header__navigation').addClass('header--fixed');

        }else{
            $('.header__navigation').removeClass('header--fixed');
        }
    });
    mobile_nav();
});
//  Slick slider
function slider (){

    var activeClass = 'slick-active',
        ariaAttribute = 'aria-hidden';
    $( '.slider-main' )
        .on( 'init', function() {
            $( '.slider-main__dots li:first-of-type' ).addClass( activeClass ).attr( ariaAttribute, false );
        } )
        .on( 'afterChange', function( event, slick, currentSlide ) {
            var $dots = $( '.slider-main__dots' );
            $( 'li', $dots ).removeClass( activeClass ).attr( ariaAttribute, true );
            $dots.each( function() {
                $( 'li', $( this ) ).eq( currentSlide ).addClass( activeClass ).attr( ariaAttribute, false );
            } );
        } );
    $('.slider-main').slick({
        dots: true,
        dotsClass: "slider-main__dots",
        appendDots: $('.slider-main'),
        infinite: true,
        fade: false,
        cssEase: 'linear',
        arrows: false
    });

    $('.news__news-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        fade: false,
        cssEase: 'linear',
        arrows: false,
        adaptiveHeight: false,
        autoplay: true,
        autoplaySpeed: 3000
    });

    $('.recommendation__list').slick({
        dots: false,
        infinite: true,
        speed: 300,
        fade: true,
        cssEase: 'linear',
        arrows: true,
        adaptiveHeight: true,
        appendArrows: $(".recommendation__control"),
        prevArrow: '<a class="recommendation__prev">Предыдущая статья</a>',
        nextArrow: '<a class="recommendation__next">Следущая статья</a>',
        responsive: [
           {
                breakpoint: 992,
                settings: {
                   arrows: false,
                   dots: true
                }
            }
        ]

    });

    $('.friends__slider').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 994,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 776,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });


    $('.slider-photo__list').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true,
        arrows: false,
        fade: true,
        asNavFor: '.slider-photo__list-for',
        responsive: [
            {
                breakpoint: 776,
                settings: {
                    dots: true
                }
            }
        ]
    });

    $('.slider-photo__list-for').slick({
        slidesToShow: 5,
        slidesToScroll: 5,
        asNavFor: '.slider-photo__list',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        infinite: true,
        swipeToSlide: true
    });

    $('.news__block').slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: true,
            centerMode: true,
            centerPadding: '0',
            touchThreshold: 2,
            infinite: false,
            swipeToSlide: true,
            useTransform: false,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },  {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },

                {
                    breakpoint: 9999,
                    settings: "unslick"
                }

            ]
        });
        $('.science__list').slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            dots: true,
            centerMode: true,
            centerPadding: '0',
            touchThreshold: 3,
            swipeToSlide: true,
            useTransform: false,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                }, {
                    breakpoint: 9999,
                    settings: "unslick"
                }


            ]
        });

        $('.reviews__list').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            centerMode: true,
            centerPadding: '0',
            swipeToSlide: true,
            useTransform: false,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 9999,
                    settings: "unslick"
                }


            ]

        });
        $('.ads__column-2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            swipeToSlide: true,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 9999,
                    settings: "unslick"
                }


            ]

        });
    }



//End slick slider



function mobile_nav() {
  if ($(window).width() <= '1083') {
      $('.mobile-nav ').append($('.user__list'));
      $('.mobile-nav').append($('.header .nav__nav-main'));
      $('.header .nav__nav-main').append($('.header .nav__info .info__item'));
      $('.mobile-nav').append($('.nav-polyclinic'));
      $(".nav-main__item").on("click",function(){
          $(this).toggleClass('active');
      });

    }
    else {
      $('.header__user .container').append($('.user__list'));
      $('.header .nav__nav-main').insertAfter($('.header .nav__go-home'));
      $('.header .nav__info').append($('.header .nav__nav-main .info__item'));
      $('.header__nav-polyclinic .container').append($('.nav-polyclinic'));
   }
}

$(document).ready(function () {
  $(".burger-btn").on("click",function(){
    $(this).toggleClass("active");
    $(".burger-btn__top").toggleClass("active");
    $(".burger-btn__bottom").toggleClass("active");
    $(".burger-btn__middle").toggleClass("active");
    $(".mobile-nav-block").toggleClass("active");
    $("body").toggleClass("active");
  });
  $(".contacts__mobile").on("click",function(){
    $(".contacts__mobile").toggleClass("active");
    $(".contacts--flex").toggleClass("active");
  });
  $(".about-us__link").on("click",function(){
    $(".about-us__text-block").toggleClass("active");
    $(".about-us__link").toggleClass("active");
  })

})






