
var toggle_work = document.querySelector(".toogle__work");
var toggle_specialists = document.querySelector(".toogle__specialists");
var work = document.querySelector(".department__work");
var specialists = document.querySelector(".department__specialists");


toggle_work.addEventListener("click", function (evt) {
    evt.preventDefault();
    toggle_work.classList.add("active");
    toggle_specialists.classList.remove("active");
    work.classList.add("block-show-work");
    specialists.classList.remove("block-show-specialists");

});
toggle_specialists.addEventListener("click", function (evt) {
    evt.preventDefault();
    toggle_work.classList.remove("active");
    toggle_specialists.classList.add("active");
    work.classList.remove("block-show-work");
    specialists.classList.add("block-show-specialists");

});
