<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="container">
        <ul class="nav-polyclinic">
            <?
            foreach($arResult as $arItem):
            ?>

                    <li class="polyclinic__item"><a class="nav-polyclinic__link link--color-white" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>

            <?endforeach?>
        </ul>
</div>
<?endif?>