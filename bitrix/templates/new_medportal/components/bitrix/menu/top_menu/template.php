<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
    <div class="container navigation--flex">
    <div class="container navigation--flex">
    <a class="nav__go-home link--color-white" href="/">
        <span class="visually-hidden"> Главная </span>
    </a>
        <ul class="nav__nav-main">

        <?foreach($arResult as $arItem):?>
            <li class="nav-main__item">
                <a class="nav-main__link link--color-white" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            </li>
        <?endforeach?>
        </ul>
        <ul class="nav__info">
            <li class="info__item">
                <a class="info__link icon-fonendoskop link--color-white" href="/schedule/record_wizard.php">Записаться на приём</a>
            </li>
            <li class="info__item">
                <a class="info__link icon-mobile link--color-white " href="/contacts"v>Контакты</a>
            </li>
        </ul>
    </div>
    </div>
<?endif?>