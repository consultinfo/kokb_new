<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <div class="container navigation--flex">
    <a class="nav__go-home link--color-white" href="/">
        <span class="visually-hidden"> Главная </span>
    </a>

<ul class="nav__nav-main">

<?
$previousLevel = 0;
foreach($arResult as $arItem):?>



	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li class="nav-main__item"><a href="<?=$arItem["LINK"]?>" class="nav-main__link link--color-white"><?=$arItem["TEXT"]?></a>
    <div class="nav-main__nav-sub">
    <div class="nav-sub__container container">
				<ul class="nav-sub__list">

		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li class="nav-main__item"><a href="<?=$arItem["LINK"]?>" class="nav-main__link link--color-white"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li class="nav-sub__item"><a class="nav-sub__link link--color-white" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></div></div></li>", ($previousLevel-1) );?>
<?endif?>

</ul>

    <ul class="nav__info">
        <li class="info__item" tabindex="0">
            <a class="info__link icon-fonendoskop link--color-white" href="/schedule/record_wizard.php">Записаться на приём</a>
        </li>
        <li class="info__item info__mobile-header" tabindex="0">
            <a class="info__link icon-mobile link--color-white" href="/contacts">Контакты</a>
        </li>
    </ul>
    <div class="burger-btn">
        <div class="burger-btn__top"></div>
        <div class="burger-btn__middle"></div>
        <div class="burger-btn__bottom"></div>
    </div>
<?endif?>