<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if($arResult["ITEMS"][0]["NAME"] == "Работа"){
    $work = $arResult["ITEMS"][0];
    $specialists = $arResult["ITEMS"][1];
}
elseif($arResult["ITEMS"][0]["NAME"] == "Специалисты"){
    $work = $arResult["ITEMS"][1];
    $specialists = $arResult["ITEMS"][0];
}

if(isset($work["NAME"]) && isset($specialists["NAME"])){?>
<div class="department__toogle">
    <a class="toggle__link toogle__work active"><?=$work["NAME"]?></a>
    <a class="toggle__link toogle__specialists "><?=$specialists["NAME"]?></a>
</div>
<?}?>

<section class="department__work block-show-work">
    <div class="work__chief clearfix">
        <img class="chief__photo" src="<?=$work["PROPERTIES"]["SUPERVISION"]["PHOTO"]?>" alt="<?=$work["PROPERTIES"]["SUPERVISION"]["NAME"]?>"/>
        <p class="chief__name"><?=$work["PROPERTIES"]["SUPERVISION"]["NAME"]?></p>
        <i class="chief__description"><?=$work["PROPERTIES"]["SUPERVISION"]["PREVIEW_DESCRIPTION"]?></i>
        <p class="chief__number-text">Телефоны отеления</p>
        <div class="chief__tel-block">
            <a class="chief__tel link"><?=$work["PROPERTIES"]["OTDEL_PHONE"]["VALUE"]?></a>
        </div>
    </div>
    <div class="work__info">
        <?=$work["PREVIEW_TEXT"]?>
    </div>
    <div class="work__info-history">
        <?=$work["DETAIL_TEXT"]?>
    </div>
    <div class="work__slider-photo">
        <ul class="slider-photo__list">
            <?foreach($work["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>
                <li class="slider-photo__item">
                    <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
                    <p class="slider-photo__text"><?=$photo["DESCRIPTION"]?></p>
                </li>
            <?endforeach;?>
        </ul>
        <ul class="slider-photo__list-for">
            <?foreach($work["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>
                <li class="slider-photo__item-for">
                    <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</section>

<section class="department__specialists">
    <ul class="specialists-list">
        <?foreach($specialists["PROPERTIES"]["STAFF"] as $employer):?>
            <li class="specialists__item">
                <div class="specialists__block-image">
                    <img class="specialists__image" src="<?=$employer["PHOTO"]?>"  alt="<?=$employer["NAME"]?>"/>
                </div>
                <h3 class="specialists__name"><?=$employer["NAME"]?></h3>
                <p class="specialists__post"><?=$employer["PREVIEW_DESCRIPTION"]?></p>
            </li>
        <?endforeach;?>
    </ul>
</section>

<script>
    var toggle_work = document.querySelector(".toogle__work");
    var toggle_specialists = document.querySelector(".toogle__specialists");
    var work = document.querySelector(".department__work");
    var specialists = document.querySelector(".department__specialists");


    toggle_work.addEventListener("click", function (evt) {
        evt.preventDefault();
        toggle_work.classList.add("active");
        toggle_specialists.classList.remove("active");
        work.classList.add("block-show-work");
        specialists.classList.remove("block-show-specialists");

    });
    toggle_specialists.addEventListener("click", function (evt) {
        evt.preventDefault();
        toggle_work.classList.remove("active");
        toggle_specialists.classList.add("active");
        work.classList.remove("block-show-work");
        specialists.classList.add("block-show-specialists");

    });
</script>






