
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if($arResult["ITEMS"][0]["NAME"] == "Работа"){
    $work = $arResult["ITEMS"][0];
    $specialists = $arResult["ITEMS"][1];
}
elseif($arResult["ITEMS"][0]["NAME"] == "Специалисты"){
    $work = $arResult["ITEMS"][1];
    $specialists = $arResult["ITEMS"][0];
}

    $manager = array_shift($work["DISPLAY_PROPERTIES"]["SUPERVISION"]["LINK_ELEMENT_VALUE"]);

    $data_of_manager = CIBlockElement::GetByID($manager["ID"]);
    if($ar_res = $data_of_manager->GetNext()):
        $boss = array (
            "NAME" => $ar_res["NAME"],
            "PHOTO" =>  CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
            "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
        );
    endif;

    foreach($specialists["PROPERTIES"]["STAFF"]["VALUE"] as $arItem):
        $employer = CIBlockElement::GetByID($arItem);
        if($ar_res = $employer->GetNext()):

         $employers[$ar_res["ID"]]= array (
             "NAME" => $ar_res["NAME"],
             "PHOTO" =>  CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
             "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
         );
        endif;
    endforeach;

if($arResult["ITEMS"][0]["NAME"] == "Работа"){
    $arResult["ITEMS"][0]["PROPERTIES"]["SUPERVISION"] = $boss;
    $arResult["ITEMS"][1]["PROPERTIES"]["STAFF"] = $employers;
}
else{
    $arResult["ITEMS"][1]["PROPERTIES"]["SUPERVISION"] = $boss;
    $arResult["ITEMS"][0]["PROPERTIES"]["STAFF"] = $employers;
}
?>






