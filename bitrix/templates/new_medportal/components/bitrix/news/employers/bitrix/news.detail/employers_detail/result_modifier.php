
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();


if(isset($arResult["DISPLAY_PROPERTIES"]["SUPERVISION"])) {
    ?><p<?
    $manager = array_shift($arResult["DISPLAY_PROPERTIES"]["SUPERVISION"]["LINK_ELEMENT_VALUE"]);

    $data_of_manager = CIBlockElement::GetByID($manager["ID"]);
    if ($ar_res = $data_of_manager->GetNext()):
        $boss = array(
            "NAME" => $ar_res["NAME"],
            "PHOTO" => CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
            "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
        );
    endif;
     $arResult["PROPERTIES"]["SUPERVISION"] = $boss;
    }


if(isset($arResult["PROPERTIES"]["STAFF"])) {

    foreach ($arResult["PROPERTIES"]["STAFF"]["VALUE"] as $arItem):
        $employer = CIBlockElement::GetByID($arItem);
        if ($ar_res = $employer->GetNext()):

            $employers[$ar_res["ID"]] = array(
                "NAME" => $ar_res["NAME"],
                "PHOTO" => CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
                "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
            );
        endif;
    endforeach;

    $arResult["PROPERTIES"]["STAFF"] = $employers;
}






