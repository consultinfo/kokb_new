<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?if(!isset($arResult["DISPLAY_PROPERTIES"]["SUPERVISION"]) && isset($arResult["DETAIL_TEXT"])):?>
    <?=$arResult["DETAIL_TEXT"]?>
<?endif?>


<section class="department__work <?if(isset($arResult["DISPLAY_PROPERTIES"]["SUPERVISION"])):?>block-show-work<?endif?>">
    <?if(isset($arResult["PROPERTIES"]["SUPERVISION"])):?>
        <div class="work__chief clearfix">
            <img class="chief__photo" src="<?=$arResult["PROPERTIES"]["SUPERVISION"]["PHOTO"]?>" alt="<?=$arResult["PROPERTIES"]["SUPERVISION"]["NAME"]?>" title="<?=$arResult["PROPERTIES"]["SUPERVISION"]["NAME"]?>" />
            <p class="chief__name"><?=$arResult["PROPERTIES"]["SUPERVISION"]["NAME"]?></p>
            <i class="chief__description"><?=$arResult["PROPERTIES"]["SUPERVISION"]["PREVIEW_DESCRIPTION"]?></i>
        </div>
    <?endif?>
        <?if (isset($arResult["DETAIL_TEXT"])):?>
            <div class="work__info-history">  <?=$arResult["DETAIL_TEXT"]?> </div>
        <?endif?>
    <div class="work__slider-photo">
        <ul class="slider-photo__list">
            <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>
                <li class="slider-photo__item">
                    <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
                    <p class="slider-photo__text"><?=$photo["DESCRIPTION"]?></p>
                </li>
            <?endforeach;?>
        </ul>
        <ul class="slider-photo__list-for">
            <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>
                <li class="slider-photo__item-for">
                    <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</section>


<section class="department__specialists <?if(isset($arResult["DISPLAY_PROPERTIES"]["STAFF"])):?>block-show-work<?endif?>">
    <ul class="specialists-list">
        <?foreach($arResult["PROPERTIES"]["STAFF"] as $employer):?>
                  <li class="specialists__item">
                <div class="specialists__block-image">
                    <img class="specialists__image" src="<?=$employer["PHOTO"]?>"  alt="<?=$employer["NAME"]?>"/>
                </div>
                <h3 class="specialists__name"><?=$employer["NAME"]?></h3>
                <p class="specialists__post"><?=$employer["PREVIEW_DESCRIPTION"]?></p>
            </li>
        <?endforeach;?>
    </ul>
</section>