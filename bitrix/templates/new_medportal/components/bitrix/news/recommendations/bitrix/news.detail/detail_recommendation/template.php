<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
    <h3 class="news-detail__title"><?=$arResult["NAME"]?></h3>
        <span class="news-detail__date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
            <div class="news-detail__image-block">
                <img class="news-detail__image" src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
            </div>
            <p class="news-detail__text">
            <?echo $arResult["DETAIL_TEXT"];?>
            </p>
    <div class="news-detail__button-block">
        <a class="button--green button" href="/recommendations/">Все объявления</a>
    </div>
