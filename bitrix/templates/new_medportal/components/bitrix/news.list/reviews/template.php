<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block--flex-row">
    <h2 class="reviews__title title--style">Отзывы пациентов</h2>
    <a class="button--green button" href="/reviews/">Все отзывы</a>
</div>

<div class="block-mobile-slider">
    <ul class="reviews__list">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <li class="reviews__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <img class="reviews__image" src="<?=SITE_TEMPLATE_PATH?>/images/review-person.png" width="58" height="64" alt="">
                <p class="reviews__name"><?=$arItem["NAME"]?></p>
                <span class="reviews__date date--style"><?=$arItem["DISPLAY_ACTIVE_FROM"]?><span class="reviews__time"></span> </span>
                    <p class="reviews__text">
                        <?= substr($arItem["DETAIL_TEXT"],0,200);?>...
                    </p>
                <a class="reviews__more-info link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">Подробнее</a>
            </li>
        <?endforeach;?>
    </ul>
</div>
