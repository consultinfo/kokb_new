<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block--flex-row">
    <h2 class="recommendation__title title--style">Врачи рекомедуют</h2>
    <a class="button--green button" href="/recommendations/">Все cтатьи</a>
</div>

<div class="recommendation__slider">
    <ul class="recommendation__list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <li class="recommendation__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="recommendation--flex">
            <div class="recommendation__image-block">
                <img class="recommendation__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"  width="458" height="300" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"">
            </div>
            <div class="recommendation__text">
                <p class="text__title text--color-light-green text--size-big">
                    <a class="text--color-light-green link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                       <?=$arItem["NAME"]?>
                    </a>
                </p>
                <p class="recommendation--mobile-none">
                    <?=$arItem["PREVIEW_TEXT"]?>
                </p>
            </div>
        </div>
    </li>
<?endforeach;?>
    </ul>
    <div class="recommendation__control">

    </div>
</div>
