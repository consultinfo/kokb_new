<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block--flex-row">
    <h2 class="ads__title title--style">Объявления</h2>
    <a class="button--green button" href="/advertisements/">Все объявления</a>
</div>
<div class="ads__mobile-slider">
    <div class="ads__column-2">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="ads__block" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <p class="ads__text ads__text--size">
                    <a class="text--color-white link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <?=$arItem["NAME"]?>
                    </a>
                </p>
                <p class="ads__text">
                    <?=$arItem["PREVIEW_TEXT"]?>
                </p>
            </div>
        <?endforeach;?>
    </div>
</div>
