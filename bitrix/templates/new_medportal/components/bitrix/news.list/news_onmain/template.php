<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container news--padding">
    <div class="news__block">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <article class="news__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="news__block-image">
        <img class="news__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="300" height="200" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
        </div>
            <p class="news__text">
            <a class="text--color-black link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
        </p>
        <span class="news__date date--style"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
    </article>
<?endforeach;?>
    </div>
</div>