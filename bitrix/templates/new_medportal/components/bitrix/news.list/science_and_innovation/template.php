<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block--flex-row">
    <h2 class="science__title title--style">Наука и инновации</h2>
    <a class="button--green button" href="/science/">Все статьи</a>
</div>

<div class="block-mobile-slider">
    <ul class="science__list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li class="science__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <p class="science__text">
                <a class="text--color-black link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <?=$arItem["NAME"]?>
                </a>
            </p>
            <span class="science__date date--style"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
        </li>
    <?endforeach;?>
    </ul>
</div>
