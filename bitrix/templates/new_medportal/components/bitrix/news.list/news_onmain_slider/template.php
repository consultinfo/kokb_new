<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block--flex-row">
    <h2 class="news__title title--style">Новости больницы</h2>
    <a class="button--green button" href="/about/news/">Все новости</a>
</div>

<div class="news__news-slider">
    <?foreach($arResult["ITEMS"] as $arItem):?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="news-slider__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <div class="news-slider__container">
            <div class="news-slider__image-block">
            <img class="news-slider__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="450" height="300" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"/>
            </div>
            <div class="news-slider__text">
                <p class="text--size-big">
                    <a class="text--color-light-green link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
                </p>
                <p class="news-slider--mobile-none"><?=$arItem["PREVIEW_TEXT"]?></p>
            </div>
        </div>
    </div>
    <?endforeach;?>
</div>