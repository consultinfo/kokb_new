<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<?if (isset($arResult["DETAIL_TEXT"])):?>
   <?=$arResult["DETAIL_TEXT"]?>
<?endif?>

<section class="department__work <?if(isset($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"])):?>block-show-work<?endif?>">
<div class="work__slider-photo">
    <ul class="slider-photo__list">

        <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>

            <li class="slider-photo__item">
                <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
                <p class="slider-photo__text"><?=$photo["DESCRIPTION"]?></p>
            </li>

        <?endforeach;?>

    </ul>
    <ul class="slider-photo__list-for">

        <?
        foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $photo):?>


            <li class="slider-photo__item-for">
                <img class="slider-photo__img" src="<?=$photo["SRC"]?>" alt="<?=$photo["DESCRIPTION"]?>"/>
            </li>


        <?endforeach;?>
    </ul>
</div>
</section>

<section class="department__work <?if(isset($arResult["PROPERTIES"]["GLAV_DOCTOR"])):?>block-show-work<?endif?>">
    <?if(isset($arResult["PROPERTIES"]["GLAV_DOCTOR"])):?>
        <div class="work__chief clearfix">
            <img class="chief__photo" src="<?=$arResult["PROPERTIES"]["GLAV_DOCTOR"]["PHOTO"]?>" alt="<?=$arResult["PROPERTIES"]["GLAV_DOCTOR"]["NAME"]?>" title="<?=$arResult["PROPERTIES"]["GLAV_DOCTOR"]["NAME"]?>" />
            <p class="chief__name"><?=$arResult["PROPERTIES"]["GLAV_DOCTOR"]["NAME"]?></p>
            <i class="chief__description"><?=$arResult["PROPERTIES"]["GLAV_DOCTOR"]["PREVIEW_DESCRIPTION"]?></i>
        </div>
    <?endif?>

</section>

<section class="department__specialists <?if(isset($arResult["PROPERTIES"]["STAFF"])):?>block-show-specialists<?endif?>">
    <ul class="specialists-list">
        <?
        foreach($arResult["PROPERTIES"]["STAFF"] as $employer):?>
            <li class="specialists__item">
                <div class="specialists__block-image">
                    <img class="specialists__image" src="<?=$employer["PHOTO"]?>"  alt="фото содрудника"/>
                </div>
                <h3 class="specialists__name"><?=$employer["NAME"]?></h3>
                <p class="specialists__post"><?=$employer["PREVIEW_DESCRIPTION"]?></p>
            </li>
        <?endforeach;?>
    </ul>
</section>







