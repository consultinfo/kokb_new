
<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

if(isset($arResult["PROPERTIES"]["GLAV_DOCTOR"]["VALUE"])) {
    $data_of_manager = CIBlockElement::GetByID($arResult["PROPERTIES"]["GLAV_DOCTOR"]["VALUE"]);

    if ($ar_res = $data_of_manager->GetNext()):
        $boss = array(
            "NAME" => $ar_res["NAME"],
            "PHOTO" => CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
            "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
        );
    endif;
}
if(isset($arResult["PROPERTIES"]["STAFF"]["VALUE"])) {
    foreach ($arResult["PROPERTIES"]["STAFF"]["VALUE"] as $arItem):
        $employer = CIBlockElement::GetByID($arItem);
        if ($ar_res = $employer->GetNext()):
            $employers[$ar_res["ID"]] = array(
                "NAME" => $ar_res["NAME"],
                "PHOTO" => CFile::GetPath($ar_res["PREVIEW_PICTURE"]),
                "PREVIEW_DESCRIPTION" => $ar_res["PREVIEW_TEXT"]
            );
        endif;
    endforeach;
}
    $arResult["PROPERTIES"]["GLAV_DOCTOR"] = $boss;
    $arResult["PROPERTIES"]["STAFF"] = $employers;






