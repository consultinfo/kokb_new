<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.css" />


<?$APPLICATION->SetTitle($arResult["SECTION"]["NAME"]);?>

<!-- photo glav -->
<?
$arIBlockElement = GetIBlockElement($arResult["PROPERTIES"]["SUPERVISION"]["VALUE"], 'foundations');
$arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
$arImgBig = CFile::GetFileArray($arIBlockElement["DETAIL_PICTURE"]);

$arImgBigResize = CFile::ResizeImageGet($arImgBig, array("width" => 600, 'height' => 450), BX_RESIZE_IMAGE_PROPORTIONAL, true);

?>
<?if(is_array($arImg)):?>
	<? $imgResize = CFile::ResizeImageGet($arImg, array("width" => 150, 'height' => 225), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
     <div id="photo_detail">
         <div class="fon">
         	<div class="img_center">
            <? if (isset($arImgBigResize) AND count($arImgBigResize) > 1) { ?>
             <a href="<?=$arImgBigResize["src"]?>" title="<?=$arIBlockElement["NAME"]?> - <?=$arIBlockElement["PREVIEW_TEXT"]?>"><img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" /></a>
            <? } else { ?>
            	<img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" />
            <? } ?>
            </div>
             <div class="fio"><?=$arIBlockElement["NAME"]?></div>
             <div class="short"><?=$arIBlockElement["PREVIEW_TEXT"]?></div>
         </div>
    </div>
<?endif;?>
<!-- description -->
<div id="text_detail">
    <?=$arResult["DETAIL_TEXT"]?>
    <?if ($arResult["PROPERTIES"]["OTDEL_PHONE"]["VALUE"] != "") {?>
	<div class="phone_otdel"><?=GetMessage("OTD_PHONE")?> <span><?=$arResult["PROPERTIES"]["OTDEL_PHONE"]["VALUE"]?></span></div>
<?}?>
</div>
<div class="br"></div><br />

<!-- sotrudnik list -->
<? if (is_array($arResult["PROPERTIES"]["STAFF"]["VALUE"]) AND count($arResult["PROPERTIES"]["STAFF"]["VALUE"]) > 0) { ?>
	<? $rez = ""; ?>
	<? foreach($arResult["PROPERTIES"]["STAFF"]["VALUE"] as $val):
			$arIBlockElement = GetIBlockElement($val, 'foundations');
			$arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
			$imgResize = CFile::ResizeImageGet($arImg, array("width" => 150, 'height' => 225), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			if ($arIBlockElement["DETAIL_TEXT"] != "") {
	?>
                <img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" class="img_fon_big" />
                <span class="name"><?=$arIBlockElement["NAME"]?></span><br />
                <?=$arIBlockElement["DETAIL_TEXT"]?>
                <div class="br"></div><br />
            <? }
			else { 
				$rez .= "<tr>
        			<td align=\"left\" valign=\"top\" width=\"150\">
            			<img src=\"".$imgResize["src"]."\" alt=\"".$arIBlockElement["NAME"]."\" title=\"".$arIBlockElement["NAME"]."\" class=\"img_fon\" />
            		</td>
					<td align=\"left\" valign=\"middle\">
					<div class=\"name\">".$arIBlockElement["NAME"]."</div>
					<div class=\"desc\">".$arIBlockElement["PREVIEW_TEXT"]."</div>
					</td>
				</tr>
        		<tr><td colspan=\"2\"><br /></td></tr>";
            } ?>   
    <?endforeach;?>
    <? if ($rez != "") {?>
			<table border="0" cellpadding="0" cellspacing="0" class="detail_specialist">
            	<?=$rez?>
            </table>
	<? }
  } ?>


<!-- photogalary -->
    <?if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["VALUE"]) > 1): ?>
        <br />
        <div id="gallery">
        	<ul>
            	<?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $key => $val):?>
                        <?	
							$imgTmpBig = CFile::ResizeImageGet($val, array("width" => 600, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
						
							$imgTmpSmall = CFile::ResizeImageGet($val, array("width" => 100, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
							if (strlen($val["DESCRIPTION"]) >= 254) {
								$val["DESCRIPTION"] .= "...";
							}
                        ?>
                        <li>
                        	<a href="<?=$imgTmpBig["src"]?>" title="<?=$val["DESCRIPTION"]?>">
                                <img src="<?=$imgTmpSmall["src"]?>" alt="" />
                            </a>
                        </li>
                <?endforeach?>
            </ul>
        </div>
        <br />
    <?else:?>
    	<? if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["VALUE"]) != 0) { ?>
    	<br />
        <div id="gallery">
        	<ul>
                <?
					$imgTmpSmall = CFile::ResizeImageGet($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"], array("width" => 100, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
					if (strlen($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"]["DESCRIPTION"]) >= 254) {
						$arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"]["DESCRIPTION"] .= "...";
					}
                 ?>
                 <li>
                    	<a href="<?=$arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"]["SRC"]?>" title="<?=$arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"]["DESCRIPTION"]?>">
                           <img src="<?=$imgTmpSmall["src"]?>" alt="" />
                       </a>
                 </li>
            </ul>
        </div>
        <br />  
        <? } ?>  
    <?endif?>
    
   <script type="text/javascript">
		$(function() {
			$('#gallery a').lightBox();
			$('#photo_detail a').lightBox();
		});
	</script>