<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult))
	return "";
$strReturn = '';
$strReturn .= '<ul class="bread-crumbs">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++) {
        $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
            $strReturn .= '
                <li class="bread-crumbs__item">
                    '.$arrow.'
                    <a href="'.$arResult[$index]["LINK"].'" class="bread-crumbs__link link" title="'.$title.'" itemprop="url">
                        <span itemprop="title">'.$title.'</span>
                    </a>
                </li>';
    }
$strReturn .= '</ul>';

return $strReturn;
