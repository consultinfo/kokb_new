<?
$MESS ['MFT_NAME'] = "ФИО";
$MESS ['MFT_EMAIL'] = "Ваш E-mail";
$MESS ['MFT_PHONE'] = "Ваш телефон";
$MESS ['MFT_ADRES'] = "Ваш адрес";
$MESS ['MFT_MESSAGE'] = "Сообщение";
$MESS ['MFT_TOPIC'] = "Тема сообщения";
$MESS ['MFT_ORG'] = "Организация";
$MESS ['MFT_FILE'] = "Файл";
$MESS ['MFT_CAPTCHA'] = "Защита от автоматических сообщений";
$MESS ['MFT_CAPTCHA_CODE'] = "Введите слово на картинке";
$MESS ['MFT_SUBMIT'] = "Отправить";
?>