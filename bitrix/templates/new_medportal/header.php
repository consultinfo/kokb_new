<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);

$page = $APPLICATION->GetCurPage(false);
define('MAIN', $page == '/');
?>

<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID;?>" lang="<?=LANGUAGE_ID;?>">
<head>
    <title><?=$APPLICATION->ShowTitle();?></title>
<?

/** fonts */
Asset::getInstance()->addString('<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet">');

/** css */
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/lib/normalize.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/lib/slick-theme.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/lib/slick.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/responsive.css",true);

/** js */
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/lib/jquery_1.11.2.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/lib/slick.min.js");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js",true);


Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');

$APPLICATION->ShowHead();
?>

</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<header class="header">

    <div class="header__logo">
        <div class="container logo--flex">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/logo.php", Array(), Array("MODE" => "html", "NAME" =>"логотип"));?>
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/site_name.php", Array(), Array("MODE" => "html", "NAME" =>"логотип"));?>
        </div>
    </div>
    <div class="header__user">
        <div class="container">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include/header_top_links.php", Array(), Array("MODE" => "html", "NAME" =>"ссылки"));?>
        </div>
    </div>
    <nav class="header__navigation">

        <?$APPLICATION->IncludeComponent(
	"bitrix:menu",
	"main_menu",
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "3",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "main_menu"
	),
	false
);?>
    </nav>
    <div class="header__nav-polyclinic">
        <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "under_main_menu",
        array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(
            ),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "undertop",
            "USE_EXT" => "N",
            "COMPONENT_TEMPLATE" => "under_main_menu"
        ),
        false
    );?>
    </div>
    <div class="mobile-nav-block">
        <div class="mobile-nav">

        </div>
    </div>
</header>
<main <?if(!MAIN):?>class="content-page"<?endif;?>>
    <?if(!MAIN){?>
    <section class="department__title department__title--padding-bottom">
        <div class="container">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "kroshki",
                Array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0",
                ),
                false
            );?>
            <h2 class="tittle__text text--margin-null"><?$APPLICATION->ShowTitle(false)?></h2>
        </div>

    </section>
    <div class="container">
    <?$white_block = $APPLICATION->GetProperty("non_white_block");
    if(empty($white_block)):?>
        <div class="white-block">
    <?endif;?>
    <?}?>