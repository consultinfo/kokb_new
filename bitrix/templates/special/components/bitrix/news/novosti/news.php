<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/*$this->setFrameMode(true);*/
use Bitrix\Iblock;
CModule::IncludeModule("iblock");
$GLOBALS["FILTER_NEWS"]["PROPERTY_STATUS"] = "";
if ($_REQUEST["NEWS"])
{	$GLOBALS["FILTER_NEWS"]=array("SECTION_ID" => $_REQUEST["NEWS"]);
    /*$all_prop = CIBlockPropertyEnum::GetList(
        array("sort" => "asc"),
        array("IBLOCK_ID" => "1", "CODE" => "FILTER")
    );
    while($prop = $all_prop->GetNext())
        $filter[$prop["EXTERNAL_ID"]] = $prop["ID"];

	$GLOBALS[ $arParams["FILTER_NAME"] ]["PROPERTY_FILTER"] = $filter[$_REQUEST["FILTER"]];*/
}
$GLOBALS["FILTER_NEWS"]["PROPERTY_STATUS"] = "";
$year=date('Y');
if (!$_REQUEST["FILTER"] && $_REQUEST["YEAR"]){
	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.01.".$_REQUEST["YEAR"],"31.12.".$_REQUEST["YEAR"],));}

if (!$_REQUEST["FILTER"] && $_REQUEST["YEAR"] && $_REQUEST["NEWS"]){
	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.01.".$_REQUEST["YEAR"],"31.12.".$_REQUEST["YEAR"]),"SECTION_ID" => $_REQUEST["NEWS"]);}

if ($_REQUEST["FILTER"] && !$_REQUEST["YEAR"]){
	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.".$_REQUEST["FILTER"].".".$year,"31.".$_REQUEST["FILTER"].".".$year,));}

if ($_REQUEST["FILTER"] && !$_REQUEST["YEAR"] && $_REQUEST["NEWS"]){
	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.".$_REQUEST["FILTER"].".".$year,"31.".$_REQUEST["FILTER"].".".$year),"SECTION_ID" => $_REQUEST["NEWS"]);}

if ($_REQUEST["FILTER"] && $_REQUEST["YEAR"] && $_REQUEST["NEWS"])
{	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.".$_REQUEST["FILTER"]." ".$_REQUEST["YEAR"],"31.".$_REQUEST["FILTER"]." ".$_REQUEST["YEAR"]),
	"SECTION_ID" => $_REQUEST["NEWS"]);
}

if ($_REQUEST["FILTER"] && $_REQUEST["YEAR"] && !$_REQUEST["NEWS"])
{	$GLOBALS["FILTER_NEWS"]=array("><DATE_ACTIVE_FROM" =>array("01.".$_REQUEST["FILTER"]." ".$_REQUEST["YEAR"],"31.".$_REQUEST["FILTER"]." ".$_REQUEST["YEAR"]));
}

/*if ($_REQUEST["PROJECT"])
{
    $GLOBALS[ $arParams["FILTER_NAME"] ]["PROPERTY_PROJECT"] = $_REQUEST["PROJECT"];
}
if ($_REQUEST["EVENT"])
{
    $GLOBALS[ $arParams["FILTER_NAME"] ]["PROPERTY_EVENT"] = $_REQUEST["EVENT"];
}*/
$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $arParams["SORT_BY1"],
		"SORT_ORDER1" => $arParams["SORT_ORDER1"],
		"SORT_BY2" => $arParams["SORT_BY2"],
		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => $arParams["FILTER_NAME"],
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
	),
	$component
);
