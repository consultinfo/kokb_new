<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<?=$arResult["FORM_NOTE"]?>
<?if ($arResult["isFormNote"] != "Y")
{
?>
<?=$arResult["FORM_HEADER"]?>
<?
?>
<table>
<?
if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
{
?>
	<tr>
		<td><?
	if ($arResult["isFormImage"] == "Y")
	{
	?>
	<a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
	<?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
	<?
	} //endif
	?>
			<p><?=$arResult["FORM_DESCRIPTION"]?></p>
		</td>
	</tr>
	<?
} // endif
	?>
</table>
<p>
    <form enctype="multipart/form-data" method="post" class="form appeal-form">
    	  	<div class="h2">Получить ответ</div>
		  	<div class="radio-block">
			    <div>
			    	<input type="radio" id="radio_1" name="radio" value="DEACT_LIST_1" checked="checked" />
			    	<label for="radio_1">В электронной форме</label>
			    </div>
			    <div>
			    	<input type="radio" id="radio_2" name="radio" value="DEACT_LIST_2" />
			    	<label for="radio_2">В письменной форме</label>
			    </div>
		   	</div>
		   	<p class="oblig-pole">Поля, помеченные звёздочкой *, обязательны для заполнения</p>
		<?
		foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
		{
		?>
			<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
			<span class="error-fld" title="<?=$arResult["FORM_ERRORS"][$FIELD_SID]?>"></span>
			<?endif;?>
			<?
				if ($arQuestion["CAPTION"] == "Почтовый индекс")
				{
					$labelId = "index";
				}
				elseif ($arQuestion["CAPTION"] == "Почтовый адрес")
				{
					$labelId = "adress";
				}
				elseif ($arQuestion["CAPTION"] == "E-Mail")
				{
					$labelId = "email";
				}
				else
				{
					$labelId = "h2";
				}

			?>
			<label class="h2" id="<?=$labelId?>">
				<?=$arQuestion["CAPTION"]?>
				<?if ($arQuestion["REQUIRED"] == "Y"):?><?="*";?><?endif;?>
			</label>
			<? if ($arQuestion["CAPTION"] == "Прикрепить файл"){?>
				<div class="form-box-submit">
                    <div class="box-input-file">
                        <div class="box-input-file-btn">
                            <div class="input-file">
                                <div class="f-btn">Добавить файл</div>
                                <input type="file" name="form_file_7" id="form_file_7"  />
                            </div>
                            <div class="input-file-res"><!--выводим результат  input[type=file] --></div>
                        </div>
                        <p>Допустимы следующие форматы файлов: <b>txt, doc, rtf, xls, pps, ppt, pdf, jpg, bmp, png, tif, gif, pcx, mp3, avi, odt, ods</b>. Размер файла не может превышать <b>1,5 Мб</b> (технические ограничения почтовой системы Государственной Думы).</p>
                    </div>
                    <input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="Отправить обращение" class="input-submit"/>
                </div><!-- /form-box-submit -->
			<?}
			else
			{?>
			<?=$arQuestion["HTML_CODE"]?>
			<?}?>
		<?
		}
		?>
	</form>
	<script type="text/javascript">
	    var objRadio = { DEACT_LIST_1:["form_text_11", "form_text_10"], DEACT_LIST_2:["form_email_9"] };
	    var arAllFields = [ "form_text_13", "form_text_12", "form_text_11", "form_email_9", "form_text_10", "form_textarea_8", "form_file_35", "form_email_36" ];
	    var fieldId = $('input[name=radio]:radio:checked').val();
		if ($('input[name=radio]:radio').length > 0) activateFields(fieldId);
		$('input[name=radio]:radio').click(function() {
		    fieldId = $(this).val();
		    activateFields(fieldId);
		});
	    function activateFields(fieldId) {
	    	if (fieldId == "DEACT_LIST_1")
	    	{
	    		$("#index").next().hide();
	    		$("#index").hide();
	    		$("#adress").next().hide();
	    		$("#adress").hide();
	    		$("#email").next().show();
	    		$("#email").show();
	    	}
	    	else if (fieldId == "DEACT_LIST_2")
	    	{
	    		$("#email").next().hide();
	    		$("#email").hide();
	    		$("#index").next().show();
	    		$("#index").show();
	    		$("#adress").next().show();
	    		$("#adress").show();
	    	}
	    	/*console.log(fieldId);
		    $.each(arAllFields, function(i, n) {
		    	console.log('' + n);
		      	$('' + n).show();
		      	$('label_' + n).show();
		    });
		    $.each(objRadio[fieldId] ,function(i, n) {
		      	$('' + n).hide();
		      	$('label_' + n).hide();
		    });*/
		};
	</script>
</p>
<?
} //endif (isFormNote)
?>