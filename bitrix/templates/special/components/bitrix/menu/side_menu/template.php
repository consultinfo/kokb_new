<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<nav id="nav">
	<?
	foreach($arResult as $arItem):?>
				<div class="item">
					<a
						<?if ($arItem["SELECTED"]):?> class="selected" <?endif?>
						href="<?=$arItem["LINK"]?>">
						<?=$arItem["TEXT"]?>
					</a>
				</div>
	<?endforeach?>
	</nav>
<?endif?>

