<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="sub_navigation">
	<div class="line">
		<ul>
			<?
			foreach($arResult as $arItem):?>
				<li><a <?if ($arItem["SELECTED"]):?> class="selected" <?endif?> href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endforeach?>
		</ul>
	</div>
</nav>
<?endif?>

