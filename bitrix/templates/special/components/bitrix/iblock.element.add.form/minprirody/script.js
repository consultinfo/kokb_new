$(document).on('submit','.online_reception_wrp form',function(e){
    var flag_complite = true;
    BX.closeWait();

    $.each($(this).find(".item").find(".value").find("#input_value"), function(){
        var input = $(this);
        var item = $(this);
        if (input.find(".starrequired").size())
        {
            return;
        }
        if(input.find("input,textarea").val().trim() == "")
        {
            flag_complite = false;
            input.addClass("error");
        }
        else if (input.hasClass("error")) {
            input.removeClass("error");
        }
    });

    if(!flag_complite)
    {
        alert("Заполните все обязательные поля!");
        e.preventDefault();
    }
});


function addFileToForm(id, number){
    var new_file = $("#file_"+id+"_"+number).find("[type='file']");
    if (new_file.size())
    {
        /*alert("тест");
          new_file.click();*/
    }
}

function AddFileName(id, val){
    val = val.split('\\');

    if (val == "") return false;

    var wrp = $("#input_file_"+id),
        wrp1 = $("#input_file1"),
        btn = wrp1.find(".type_file"),
        number = btn.data("number")*1,
        next_number = number + 1,
        input_html = ''+
            '<div id="file_'+id+'_'+next_number+'">'+
            '<input type="hidden" name="PROPERTY['+id+']['+next_number+']">'+
            '<input type="file" name="PROPERTY_FILE_'+id+'_'+next_number+'" onchange="AddFileName('+id+',this.value);">'+
            '</div>',
        html = ''+
                 '<div class="file" id="file_'+number+'">'+
                        '<a href="javascript:void(0);" title="Для удаления нажмите на ссылку" onclick="removeFile('+id+','+number+');">'+val+'</a>'+
                        '<span class="file_size"></span>'+
                 '</div>';

    btn.data("number", next_number);
    wrp.find(".elms").append(html);
    wrp1.find(".hide").append(input_html);
    btn.attr("onclick", 'addFileToForm('+id+','+next_number+');');

    return false;
}

function removeFile(id, number) {
    $("#file_"+number).remove();
    $("#file_"+id+"_"+number).remove();

    return false;
}