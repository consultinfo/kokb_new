<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if(array_key_exists("HIDDEN", $arItem)):?>
				<?=$arItem["INPUT"]?>
			<?elseif ($arItem["TYPE"] == "DATE_RANGE"):?>
							<?$data=explode(".",$arResult["ITEMS"]["DATE_ACTIVE_FROM"]["INPUT_VALUES"][0]);
							  $data2=explode(".",$arResult["ITEMS"]["DATE_ACTIVE_FROM"]["INPUT_VALUES"][1]);?>
												<span>с  </span>
												<select name="day" id="SelectDay" onchange="GetData()" >
													<? for($i=1;$i<=31;$i++){?>
													<option <?
															if ($data[0]==$i){echo "selected "; }?>VALUE="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>
												<select name="month" id="SelectMonth" onchange="GetData()" >
													<option <?
													if ($data[1]=="01"){echo "selected "; }?>VALUE="01">Январь</option>
													<option <?
													if ($data[1]=="02"){echo "selected "; }?>VALUE="02">Февраль</option>
													<option <?
													if ($data[1]=="03"){echo "selected "; }?>VALUE="03">Март</option>
													<option <?
													if ($data[1]=="04"){echo "selected "; }?>VALUE="04">Апрель</option>
													<option <?
													if ($data[1]=="05"){echo "selected "; }?>VALUE="05">Май</option>
													<option <?
													if ($data[1]=="06"){echo "selected "; }?>VALUE="06">Июнь</option>
													<option <?
													if ($data[1]=="07"){echo "selected "; }?>VALUE="07">Июль</option>
													<option <?
													if ($data[1]=="08"){echo "selected "; }?>VALUE="08">Август</option>
													<option <?
													if ($data[1]=="09"){echo "selected "; }?>VALUE="09">Сентябрь</option>
													<option <?
													if ($data[1]=="10"){echo "selected "; }?>VALUE="10">Октябрь</option>
													<option <?
													if ($data[1]=="11"){echo "selected "; }?>VALUE="11">Ноябрь</option>
													<option <?
													if ($data[1]=="12"){echo "selected "; }?>VALUE="12">Декабрь</option>
												</select>


												<select name="year" id="SelectYear" onchange="GetData()" >
													<? for($i=2016;$i<=2018;$i++){?>
														<option <?
																if ($data[2]==$i){echo "selected "; }?>VALUE="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>
												<br>
												<span>по</span>
												<select name="day2" id="SelectDay2" onchange="GetData2()" >
													<? for($i=1;$i<=31;$i++){?>
													<option <? if ($data2[0]==$i){echo "selected "; }?>VALUE="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>
								
													<select name="month2" id="SelectMonth2" onchange="GetData2()" >
														<option <?
																if ($data2[1]=="01"){echo "selected "; }?>VALUE="01">Январь</option>
														<option <?
																if ($data2[1]=="02"){echo "selected "; }?>VALUE="02">Февраль</option>
														<option <?
																if ($data2[1]=="03"){echo "selected "; }?>VALUE="03">Март</option>
														<option <?
																if ($data2[1]=="04"){echo "selected "; }?>VALUE="04">Апрель</option>
														<option <?
																if ($data2[1]=="05"){echo "selected "; }?>VALUE="05">Май</option>
														<option <?
																if ($data2[1]=="06"){echo "selected "; }?>VALUE="06">Июнь</option>
														<option <?
																if ($data2[1]=="07"){echo "selected "; }?>VALUE="07">Июль</option>
														<option <?
																if ($data2[1]=="08"){echo "selected "; }?>VALUE="08">Август</option>
														<option <?
																if ($data2[1]=="09"){echo "selected "; }?>VALUE="09">Сентябрь</option>
														<option <?
																if ($data2[1]=="10"){echo "selected "; }?>VALUE="10">Октябрь</option>
														<option <?
																if ($data2[1]=="11"){echo "selected "; }?>VALUE="11">Ноябрь</option>
														<option <?
																if ($data2[1]=="12"){echo "selected "; }?>VALUE="12">Декабрь</option>
													</select>
												<select name="year2" id="SelectYear2" onchange="GetData2()" >
													<? for($i=2016;$i<=2018;$i++){?>
														<option <?
													if ($data2[2]==$i){echo "selected "; }?>VALUE="<?=$i?>"><?=$i?></option>
													<?}?>
												</select>
									<input
										type="hidden"
										value="<?=$arResult["ITEMS"]["DATE_ACTIVE_FROM"]["INPUT_VALUES"][0]?>"
										name="<?=$arItem["INPUT_NAMES"][0]?>"
										placeholder=""
										id="sample1"
									/>
									<input
										type="hidden"
										value="<?=$arResult["ITEMS"]["DATE_ACTIVE_FROM"]["INPUT_VALUES"][1]?>"
										name="<?=$arItem["INPUT_NAMES"][1]?>"
										placeholder=""
										id="sample2"
									/>
			<?elseif ($arItem["TYPE"] == "select"):
				?>
				<div class="col-sm-6 col-md-4 bx-filter-parameters-box active">
					<div class="bx-filter-parameters-box-title"><span><?=$arItem["NAME"]?></span></div>
					<div class="bx-filter-block">
						<div class="row bx-filter-parameters-box-container">
							<div class="col-xs-12 bx-filter-parameters-box-container-block">
								<div class="bx-filter-input-container">
									<select name="<?=$arItem["INPUT_NAME"]?>">
										<?foreach ($arItem["LIST"] as $key => $value):?>
											<option
												value="<?=htmlspecialcharsBx($key)?>"
												<?if ($key == $arItem["INPUT_VALUE"]) echo 'selected="selected"'?>
											><?=htmlspecialcharsEx($value)?></option>
										<?endforeach?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?elseif ($arItem["TYPE"] == "CHECKBOX"):
				?>
				<div class="col-sm-6 col-md-4 bx-filter-parameters-box active">
					<div class="bx-filter-parameters-box-title"><span><?=$arItem["NAME"]?></span></div>
					<div class="bx-filter-block">
						<div class="row bx-filter-parameters-box-container">
							<div class="col-xs-12 bx-filter-parameters-box-container-block">
							<?
							$arListValue = (is_array($arItem["~INPUT_VALUE"]) ? $arItem["~INPUT_VALUE"] : array($arItem["~INPUT_VALUE"]));
							foreach ($arItem["LIST"] as $key => $value):?>
							<div class="checkbox">
								<label class="bx-filter-param-label">
									<input
										type="checkbox"
										value="<?=htmlspecialcharsBx($key)?>"
										name="<?echo $arItem["INPUT_NAME"]?>[]"
										<?if (in_array($key, $arListValue)) echo 'checked="checked"'?>
									>
									<span class="bx-filter-param-text"><?=htmlspecialcharsEx($value)?></span>
								</label>
							</div>
							<?endforeach?>
							</div>
						</div>
					</div>
				</div>
			<?elseif ($arItem["TYPE"] == "RADIO"):
				?>
				<div class="col-sm-6 col-md-4 bx-filter-parameters-box active">
					<div class="bx-filter-parameters-box-title"><span><?=$arItem["NAME"]?></span></div>
					<div class="bx-filter-block">
						<div class="row bx-filter-parameters-box-container">
							<div class="col-xs-12 bx-filter-parameters-box-container-block">
								<?
								$arListValue = (is_array($arItem["~INPUT_VALUE"]) ? $arItem["~INPUT_VALUE"] : array($arItem["~INPUT_VALUE"]));
								foreach ($arItem["LIST"] as $key => $value):?>
								<div class="radio">
									<label class="bx-filter-param-label">
										<input
											type="radio"
											value="<?=htmlspecialcharsBx($key)?>"
											name="<?echo $arItem["INPUT_NAME"]?>"
											<?if (in_array($key, $arListValue)) echo 'checked="checked"'?>
										>
										<span class="bx-filter-param-text"><?=htmlspecialcharsEx($value)?></span>
									</label>
								</div>
								<?endforeach?>
							</div>
						</div>
					</div>
				</div>
			<?else:?>
				<div class="col-sm-6 col-md-4 bx-filter-parameters-box active">
					<div class="bx-filter-parameters-box-title"><span><?=$arItem["NAME"]?></span></div>
					<div class="bx-filter-block">
						<div class="row bx-filter-parameters-box-container">
							<div class="col-xs-12 bx-filter-parameters-box-container-block">
								<div class="bx-filter-input-container">
									<?=$arItem["INPUT"]?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?endif?>
		<?endforeach;?>
		<div class="elm ctrl">
				<input type="submit" name="set_filter"  value="Показать новости" class="button" />
				<input type="hidden" name="set_filter" value="Y" />
				<!--<input type="submit" name="del_filter" value="<?//=GetMessage("CT_BCF_DEL_FILTER")?>" />-->
		</div>
<div class="clb"></div>
</form>
	<script>
		function GetData() {
			var selind = document.getElementById("SelectDay").options.selectedIndex;
			var txt = document.getElementById("SelectDay").options[selind].text;
			var valDay = document.getElementById("SelectDay").options[selind].value;

			var selind = document.getElementById("SelectMonth").options.selectedIndex;
			var txt = document.getElementById("SelectMonth").options[selind].text;
			var valMonth = document.getElementById("SelectMonth").options[selind].value;

			var selind = document.getElementById("SelectYear").options.selectedIndex;
			var txt = document.getElementById("SelectYear").options[selind].text;
			var valYear = document.getElementById("SelectYear").options[selind].value;

			document.getElementById("sample1").value = valDay+'.'+valMonth+'.'+valYear;
		}
		function GetData2() {
			var selind = document.getElementById("SelectDay2").options.selectedIndex;
			var txt = document.getElementById("SelectDay2").options[selind].text;
			var valDay1 = document.getElementById("SelectDay2").options[selind].value;

			var selind = document.getElementById("SelectMonth2").options.selectedIndex;
			var txt = document.getElementById("SelectMonth2").options[selind].text;
			var valMonth1 = document.getElementById("SelectMonth2").options[selind].value;

			var selind = document.getElementById("SelectYear2").options.selectedIndex;
			var txt = document.getElementById("SelectYear2").options[selind].text;
			var valYear1 = document.getElementById("SelectYear2").options[selind].value;

			document.getElementById("sample2").value = valDay1+'.'+valMonth1+'.'+valYear1;
		}
	</script>
<?
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/formstyler.css');
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/formstyler.js');?>