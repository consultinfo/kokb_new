<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/slider.js");?>
<div style="float: right; width: 650px;">
	<div class="photo-detail"></div>
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="data-table">
<tr>
	<td align="center">
		<?if(is_array($arResult["PICTURE"])):?>

			<div class="item">
				<a href="<?=$arResult["PICTURE"]["SRC"]?>" data-lightbox="roadtrip" data-title="<?=$arResult["DETAIL_TEXT"]?>">
				<img
				border="0"
				src="<?=$arResult["PICTURE"]["SRC"]?>"
				width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>"
				height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>"
				alt="<?=$arResult["PICTURE"]["ALT"]?>"
				title="<?=$arResult["PICTURE"]["TITLE"]?>"
				class="img_main"
				/><br /> </a>
		<?endif?> </div>
	</td>
</tr>
</table>
<?if (!empty($arResult["PROPERTIES"]["PHOTOS"]["VALUE"])) {?>
	<div id="main">
	<div class="wrap-items">
			<div class="slider">
				<div class="slide-list">
					<div class="slide-wrap">
						<?$i=0;?>
						<?foreach ((array)$arResult["PROPERTIES"]["PHOTOS"]["VALUE"] as $id) {?>
							<div class="slide-item">
							<div class="item">
								<a href="<?=CFile::GetPath($id);?>" data-lightbox="roadtrip" data-title="<?=$arResult["PROPERTIES"]["PHOTOS"]["DESCRIPTION"][$i]?>"  style="width:185px;height:180px; float: left;">
									<img src="/bitrix/templates/consultinfo/images/etc/transparent.gif"
										 alt=""
										 title="Нажмите для просмотра в полноэкранном режиме"
										 style="width:185px;height:180px;background-size:cover;background-image:url(<?=CFile::GetPath($id);?>);">
								</a>
							</div>
						</div>
						<? $i++;}?>
					</div>
				</div>
				<div class="navy prev-slide"></div>
				<div class="navy next-slide"></div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<?}?>
</div>
<script>
	$(function() {
		$(".wrap-items .item a").fancybox({
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'titlePosition' 	: 'over'
		});
	});

	lightbox.option({
		'albumLabel':  	"Фото %1 из %2"
	})

</script>