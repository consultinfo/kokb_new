<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="news_detail">
	<h4 class="news_title">
		<?=$arResult["NAME"]?>
	</h4>
	<div class="detail_news_text fix">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<figure class="image">
			<div class="table">
				<div class="td type_1">
						<img
							src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
							alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
							title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
							id="news_detail_img"
						/>
				</div>
			</div>
		</figure>
		<?endif?>
		<div class="date">
		<span class="value">
			<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
				<?=$arResult["DISPLAY_ACTIVE_FROM"]?>
			<?endif;?>
		</span>
		</div>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
			<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
		<?endif;?>
		<?if($arResult["NAV_RESULT"]):?>
			<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
			<?echo $arResult["NAV_TEXT"];?>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
		<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
			<?echo $arResult["DETAIL_TEXT"];?>
		<?else:?>
			<?echo $arResult["PREVIEW_TEXT"];?>
		<?endif?>
	</div>
	<p><a href="<?echo $arResult["LIST_PAGE_URL"];?>">Все новости</a></p>
	<div class="news_controls">
		<span>
			<? if(empty($arResult["TOLEFT"])){?>
			
					<span class="ico news_prev"></span>
					<span class="value"><strong>Предыдущая новость</strong></span>
				
			<?} else {?>
				<a href="<?=$arResult["TOLEFT"]["URL"]?>" title="<?=$arResult["TOLEFT"]["NAME"]?>">
					<span class="ico news_prev"></span>
					<span class="value"><strong>Предыдущая новость</strong></span>
				</a>
			<?}?>
	
		
			<? if(empty($arResult["TORIGHT"])){?>
					<span class="value"><strong>Следующая новость</strong></span>
					<span class="ico news_next"></span>
			<?} else {?>
				<a href="<?=$arResult["TORIGHT"]["URL"]?>" title="<?=$arResult["TORIGHT"]["NAME"]?>">
					<span class="value"><strong>Следующая новость</strong></span>
					<span class="ico news_next"></span>
				</a>
			<?}?>
		</span>
	</div>
</div>