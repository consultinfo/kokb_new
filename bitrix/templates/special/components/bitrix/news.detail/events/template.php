<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="ads_detail">
	<div class="ads_wrp">
		<div class="date">
			<span class="value">
				<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
					<span class="day"><?=$arResult["DAY"]?></span>
					<span class="month"><?=$arResult["MONTH"]?> <?=$arResult["YEAR"]?></span>
				<?endif;?>
			</span>
		</div>
		<div class="details">
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
					<img
						src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
						alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
						title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
						class="pic type_3"
					/>
			<?endif?>
			<h4><?=$arResult["NAME"]?></h4>
			<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
				<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
			<?endif;?>
			<?if($arResult["NAV_RESULT"]):?>
				<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
				<?echo $arResult["NAV_TEXT"];?>
				<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
			<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
				<?echo $arResult["DETAIL_TEXT"];?>
			<?else:?>
				<?echo $arResult["PREVIEW_TEXT"];?>
			<?endif?>
		</div>
	</div>
		<div class="news_controls">
			<div class="item">
				<? if(empty($arResult["TOLEFT"])){?>
					<div class='no_link'>
						<span class="ico news_prev"></span>
						<span class="value">Предыдущая новость</span>
					</div>
				<?} else {?>
					<a href="<?=$arResult["TOLEFT"]["URL"]?>" title="<?=$arResult["TOLEFT"]["NAME"]?>">
						<span class="ico news_prev"></span>
						<span class="value">Предыдущая новость</span>
					</a>
				<?}?>
			</div>
			<div class="item">
				<? if(empty($arResult["TORIGHT"])){?>
					<div class='no_link'>
						<span class="value">Следующая новость</span>
						<span class="ico news_next"></span>
					</div>
				<?} else {?>
					<a href="<?=$arResult["TORIGHT"]["URL"]?>" title="<?=$arResult["TORIGHT"]["NAME"]?>">
						<span class="value">Следующая новость</span>
						<span class="ico news_next"></span>
					</a>
				<?}?>
			</div>
		</div>
</div>