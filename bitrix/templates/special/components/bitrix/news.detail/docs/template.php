<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="docs_detail fix">
	<h4 class="label">Введение</h4>
	<div class="download">
		<div class="icon">
			<span class="ico docs_list"></span>
		</div>
		<div class="details">
			<div class="link">
				<a href="<?=$arResult['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">
					<?=$arResult["NAME"];?>
				</a>
			</div>
			<div class="doc_data">
				<span class="value">Скачать файл <span class="upp"><?=$arResult['FILE_TYPE']?></span> - размер: <?=$arResult['FILE_SIZE']?></span>
			</div>
		</div>
	</div>
	<?=$arResult["DETAIL_TEXT"]?>
	<div class="news_controls">
			<div class="item">
				<? if(empty($arResult["TOLEFT"])){?>
					<div class='no_link'>
						<span class="ico news_prev"></span>
						<span class="value">Предыдущий документ</span>
					</div>
				<?} else {?>
					<a href="<?=$arResult["TOLEFT"]["URL"]?>" title="<?=$arResult["TOLEFT"]["NAME"]?>">
						<span class="ico news_prev"></span>
						<span class="value">Предыдущий документ</span>
					</a>
				<?}?>
			</div>
			<div class="item">
				<? if(empty($arResult["TORIGHT"])){?>
					<div class='no_link'>
						<span class="value">Следующий документ</span>
						<span class="ico news_next"></span>
					</div>
				<?} else {?>
					<a href="<?=$arResult["TORIGHT"]["URL"]?>" title="<?=$arResult["TORIGHT"]["NAME"]?>">
						<span class="value">Следующий документ</span>
						<span class="ico news_next"></span>
					</a>
				<?}?>
			</div>
	</div>
</div>