<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
if (empty($arResult["ITEMS"])) return ""; ?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	$file_type = explode("/", $arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['CONTENT_TYPE']);
	$FILE_SIZE=CFile::FormatSize(
		$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['FILE_SIZE'],
		$precision = 1
	);
	?>
	<div class="main-news-it" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
			<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
				<h2><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h2>
			<?else:?>
				<?=$arItem["NAME"]?>
			<?endif;?>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
			<p class="news-signature"><?=$arItem["PREVIEW_TEXT"];?></p>
		<?endif;?>
		<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['ORIGINAL_NAME']?> - (<?=$FILE_SIZE?>)
		<br>
		<a class="news-button" href="<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">Скачать Документ</a>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
			<span><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif?>
	</div>
	<hr>
<?endforeach;?>
	<br /><?=$arResult["NAV_STRING"]?>