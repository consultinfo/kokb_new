<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="news">
	<div class="line">
			<h2 class="heading"><a href="<?echo $arResult["LIST_PAGE_URL"];?>"><?=$arParams["TITLE"]?></a></h2>
				<div class="items_wrap fix">
			<?foreach($arResult["ITEMS"] as $arItem):
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$arSectionImage = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 394, 'height' => 214), BX_RESIZE_IMAGE_EXACT, true);
				?>
				<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<figure class="image">
						<div class="table">
							<div class="td type_1">
								<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="block">
									<img src="<?=$arSectionImage['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="pic type_1">
								</a>
							</div>
						</div>
					</figure>
					<div class="details">

						<div class="link">
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
						</div>
						<div class="preview_text">
							<p><?=$arItem["PREVIEW_TEXT"]?></p>
						</div>
                        <div class="date">
                            <span class="value"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
                        </div>
					</div>
					</div>
			<?endforeach;?>
		   </div>
		</div>
	</div>
</div>
