<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
			<?foreach($arResult["ITEMS"] as $arItem):
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				$count=count($arItem['PROPERTIES']['full_name']['VALUE'])-1;
			?>
			<table border="4">
				<tbody>
				<tr>
					<h4><?=$arItem['NAME']?></h4>
				</tr>
				<tr>
					<td>Фамилия</td>
					<td>Должность</td>
					<td>Контакты</td>
					<td>Кабинет</td>
				</tr>
				<?for ($i=0; $i <= $count; $i++) {
					$full_name=explode(" ",$arItem['PROPERTIES']['full_name']['VALUE'][$i]);
					$surname=$full_name[0];
					$name=$full_name[1]." ".$full_name[2]." ".$full_name[3];
				?>
				<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<td>
							<span><?=$surname?></span>
							<span><?=$name?></span>		
						</td>
						<td>
							<?=$arItem['PROPERTIES']['position']['VALUE'][$i]?>
						</td>
						<td>
							<?if($arItem['PROPERTIES']['phone']['VALUE'][$i]!=" " && !empty($arItem['PROPERTIES']['phone']['VALUE'][$i])):?>
								<span class="val">Тел: <a href="tel:+<?=$arItem['PROPERTIES']['phone']['VALUE'][$i]?>"><?=$arItem['PROPERTIES']['phone']['VALUE'][$i]?></a></span><br>
							<?endif;?>
							<?if($arItem['PROPERTIES']['fax']['VALUE'][$i]!=" " && !empty($arItem['PROPERTIES']['fax']['VALUE'][$i])):?>
								<span class="val">Факс: <?=$arItem['PROPERTIES']['fax']['VALUE'][$i]?></span><br>
							<?endif;?>
							<?if($arItem['PROPERTIES']['email']['VALUE'][$i]!=" " && !empty($arItem['PROPERTIES']['email']['VALUE'][$i])):?>
								<span class="val">e-mail: <a href="mailto:<?=$arItem['PROPERTIES']['email']['VALUE'][$i]?>"><?=$arItem['PROPERTIES']['email']['VALUE'][$i]?></a></span><br>
							<?endif;?>
						</td>		
						<td>
							<span class="val"><?=$arItem['PROPERTIES']['cabinet']['VALUE'][$i]?></span>
						</td>
				</tr>
				<?}?>
				</tbody>
			</table>
			<?endforeach;?>
