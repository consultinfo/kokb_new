<?
use Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */

foreach($arResult["ITEMS"] as &$arItem)
{
	if ($arItem["DISPLAY_PROPERTIES"]["EVENT"])
	{
		$id = $arItem["DISPLAY_PROPERTIES"]["EVENT"]["VALUE"];
		//$el = $arItem["DISPLAY_PROPERTIES"]["EVENT"]["LINK_ELEMENT_VALUE"][$id];
		$db = CIBlockElement::GetProperty(2, $id, array(), Array("CODE"=>"TYPE"));
		$prop = $db->Fetch();
		$arItem["DISPLAY_PROPERTIES"]["EVENT"]["NAME"] = $prop["VALUE_ENUM"];

	}
}

global $APPLICATION;
if (strripos($APPLICATION->GetCurPage(false), "/zhizn-fonda/proekty/") !== false)
{
    $arResult["LIST_PAGE_URL"] .= "?FILTER=proekty&PROJECT=" . $GLOBALS["EVENT_NEWS"]["PROPERTY_PROJECT"];
}
else if (strripos($APPLICATION->GetCurPage(false), "/zhizn-fonda/aktsii-i-meropriyatiya/") !== false)
{
    if ($APPLICATION->GetPageProperty("EVENT_TYPE") == "event")
    {
        $type = "meropriyatiya";
    }
    else
    {
        $type = "aktsii";
    }

    $arResult["LIST_PAGE_URL"] .= "?FILTER=" . $type . "&EVENT=" . $GLOBALS["EVENT_NEWS"]["PROPERTY_EVENT"];
}



