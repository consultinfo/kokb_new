<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (empty($arResult["ITEMS"])) return ""; ?>
<section class="docs">
	<h2 class="heading"><?=$arParams["TITLE"]?></h2>
	<div class="items_wrap">
		<?foreach($arResult["ITEMS"] as $arItem):
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="icon">
					<span class="ico doc"></span>
				</div>
				<div class="details">
					<div class="date"><span class="value"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
					</div>
					<div class="link"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
				</div>
			</div>
		<?endforeach;?>
	</div>
		<?if ($arParams["SHOW_BTN_ALL_NEWS"] == "Y") {?>
			<div class="more">
				<a href="<?=$arResult["LIST_PAGE_URL"]?>" class="block">
					<span class="ico docs"></span>
					<span class="value">Все документы</span>
				</a>
			</div>
		<?}?>
</section>