<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="schedule_wrp">
	<table>
		<thead>
		<tr>
			<th colspan="2"><?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/include_areas/table_name1.php", Array(), Array("MODE" => "html", "SHOW_BORDER" => true));?></th>
		</tr>
		</thead>
		<tbody>
			<?foreach($arResult["ITEMS"] as $arItem):
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<tr id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<td class="grey"><?=$arItem['PROPERTIES']['column1']['VALUE']?></td>
				<td class="white">
					<?if (!empty($arItem['PROPERTIES']['column2_time']['VALUE']))
					{
						$full_time = explode(" ",$arItem['PROPERTIES']['column2_time']['VALUE']);
						echo $full_time[0].'<sup>'.$full_time[1].'</sup> - '.$full_time[3].'<sup>'.$full_time[4].'</sup><br>';
					} ?>
					<?if (!empty($arItem['PROPERTIES']['column2_text']['VALUE'])){echo $arItem['PROPERTIES']['column2_text']['VALUE'].'<br>';}?>
					<?if (!empty($arItem['PROPERTIES']['column2_phone']['VALUE']))
					{	foreach ($arItem['PROPERTIES']['column2_phone']['VALUE'] as $arPhone)
						{
							?><a href="tel:<?=$arPhone?>"><?=$arPhone?></a> <?
						}
					}?>
				</td>
			</tr>
			<?endforeach;?>
		</tbody>
	</table>
</div>

