<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/responsiveTabs.js');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/css/responsiveTabs.css');
if (empty($arResult["ITEMS"])) return ""; ?>
<div id="docs">
		<ul class="resp-tabs-list doc_list">
				<li>Министерство природынх ресурсов и экологии КО</li>
				<li>Департамент окружающей среды и экологического надзора</li>
				<li>Депратамент недропользования и водопользования</li>
				<li>Депертамент лесного хозяйства и использования объектов животного мира</li>
		</ul>
		<div class="resp-tabs-container doc_list">
			<div class="inner_wrp">
				<?foreach($arResult["ITEMS"] as $arItem):
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					$file_type = explode("/", $arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['CONTENT_TYPE']);
					$FILE_SIZE=CFile::FormatSize(
						$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['FILE_SIZE'],
						$precision = 1
					);
					?>
					<?if($arItem['DISPLAY_PROPERTIES']['TYPE']['VALUE_XML_ID']==1){?>
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="icon">
								<span class="ico docs_list"></span>
							</div>
							<div class="details">
								<div class="date">
									<span class="value"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
								</div>
								<div class="link">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
										<?=$arItem["NAME"]?>
									</a>
								</div>
								<div class="doc_data">
									<span class="value"><?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['ORIGINAL_NAME']?> - (<?=$FILE_SIZE?>)</span>
									<a href="<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">  Скачать</a>
								</div>
							</div>
						</div>
					<?}?>
				<?endforeach;?>
			</div>
			<div class="inner_wrp">
				<?foreach($arResult["ITEMS"] as $arItem):
					$file_type = explode("/", $arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['CONTENT_TYPE']);
					$FILE_SIZE=CFile::FormatSize(
						$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['FILE_SIZE'],
						$precision = 1);
					?>
					<?if($arItem['DISPLAY_PROPERTIES']['TYPE']['VALUE_XML_ID']==2){?>
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="icon">
								<span class="ico docs_list"></span>
							</div>
							<div class="details">
								<div class="date">
									<span class="value"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
								</div>
								<div class="link">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
										<?=$arItem["NAME"]?>
									</a>
								</div>
								<div class="doc_data">
									<span class="value"><?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['ORIGINAL_NAME']?> - (<?=$FILE_SIZE?>)</span>
									<a href="<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">  Скачать</a>
								</div>
							</div>
						</div>
					<?}?>
				<?endforeach;?>
			</div>
			<div class="inner_wrp">
				<?foreach($arResult["ITEMS"] as $arItem):

					$file_type = explode("/", $arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['CONTENT_TYPE']);
					$FILE_SIZE=CFile::FormatSize(
						$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['FILE_SIZE'],
						$precision = 1);
					?>
					<?if($arItem['DISPLAY_PROPERTIES']['TYPE']['VALUE_XML_ID']==3){?>
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="icon">
								<span class="ico docs_list"></span>
							</div>
							<div class="details">
								<div class="date">
									<span class="value"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
								</div>
								<div class="link">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
										<?=$arItem["NAME"]?>
									</a>
								</div>
								<div class="doc_data">
									<span class="value"><?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['ORIGINAL_NAME']?> - (<?=$FILE_SIZE?>)</span>
									<a href="<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">  Скачать</a>
								</div>
							</div>
						</div>
					<?}?>
				<?endforeach;?>
			</div>
			<div class="inner_wrp">
				<?foreach($arResult["ITEMS"] as $arItem):
					$file_type = explode("/", $arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['CONTENT_TYPE']);
					$FILE_SIZE=CFile::FormatSize(
						$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['FILE_SIZE'],
						$precision = 1);
					?>
					<?if($arItem['DISPLAY_PROPERTIES']['TYPE']['VALUE_XML_ID']==4){?>
						<div class="item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="icon">
								<span class="ico docs_list"></span>
							</div>
							<div class="details">
								<div class="date">
									<span class="value"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span>
								</div>
								<div class="link">
									<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
										<?=$arItem["NAME"]?>
									</a>
								</div>
								<div class="doc_data">
									<span class="value">
										<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['ORIGINAL_NAME']?> - (<?=$FILE_SIZE?>)
										<a href="<?=$arItem['DISPLAY_PROPERTIES']['DOCS']['FILE_VALUE']['SRC']?>">  Скачать</a>
									</span>
								</div>
							</div>
						</div>
					<?}?>
				<?endforeach;?>
			</div>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
				<br /><?=$arResult["NAV_STRING"]?>
			<?endif;?>
		</div>
</div>
