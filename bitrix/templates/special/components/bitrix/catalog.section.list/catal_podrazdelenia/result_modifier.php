<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$CAT_ID = IntVal($_GET["CAT_ID"]);

$arFilter = array("PROPERTY_DEFAUL_LOOK" => "18");
foreach($arResult["SECTIONS"] as $key => $arSection):
	if ($arSection["ELEMENT_CNT"] > 1) {
		$items = GetIBlockElementList(5, $arSection["ID"], Array("id" => "DESC"), 1, $arFilter);
		if (count($items->arResult) > 0) {
			$arResult["SECTIONS"][$key]["SECTION_PAGE_URL"] = "/employees/?CAT_ID=".$arSection["ID"]."&EL_ID=".$items->arResult[0]["ID"];
		}
	}
	else {
		$items = GetIBlockElementList(5, $arSection["ID"], Array("id" => "DESC"), 1);
		$arItem = $items->GetNext();
		$arResult["SECTIONS"][$key]["SECTION_PAGE_URL"] = $arItem["DETAIL_PAGE_URL"];
	}
	
	$selected = "N";
	if ($CAT_ID == $arSection["ID"]) {
		$selected = "Y";
		$APPLICATION->SetTitle($arSection["NAME"]);
	}
	$arResult["SECTIONS"][$key]["ACTIVE"] = $selected;
endforeach;

?>