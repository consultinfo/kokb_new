<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"));
	if($CURRENT_DEPTH<$arSection["DEPTH_LEVEL"])
		echo "";
	elseif($CURRENT_DEPTH>$arSection["DEPTH_LEVEL"])
		echo str_repeat("", $CURRENT_DEPTH - $arSection["DEPTH_LEVEL"]);
	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
?>
 <div class="media__item">

          <a href="<?=$arSection["SECTION_PAGE_URL"]."&special=y"?>" class="media__item-img-wrap">
		  <span class="media__item-mark photo"></span><img src="<?=$arSection["PICTURE"]["SRC"]?>" alt=""/></a>
          <h3><a href="<?=$arSection["SECTION_PAGE_URL"]."&special=y"?>"><?=$arSection["NAME"]?></a></h3>
        </div>
		<?endforeach?>


