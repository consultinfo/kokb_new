<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="catal">
<?
$more = "";
$CURRENT_DEPTH=$arResult["SECTION"]["DEPTH_LEVEL"]+1;
foreach($arResult["SECTIONS"] as $arSection):
	if($CURRENT_DEPTH<$arSection["DEPTH_LEVEL"])
		echo "<ul>";
	elseif($CURRENT_DEPTH>$arSection["DEPTH_LEVEL"])
		echo str_repeat("</ul>", $CURRENT_DEPTH - $arSection["DEPTH_LEVEL"]);
	$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
	
	$active = "";
	if ($arSection["ACTIVE"] == "Y") {
		$active = "class=\"active\"";
	}
	if ($arSection["ID"] == 99 OR $arSection["ID"] == 97 OR $arSection["ID"] == 96 OR $arSection["ID"] == 98) {
		$more .= "<li ".$active."><a href=\"".$arSection["SECTION_PAGE_URL"]."\">".$arSection["NAME"]."</a></li>";
	}
	else {
?>	
	<li <?=$active?>><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a></li>
    <? } ?>
<?endforeach?>
</ul>
<? if ($more != "") {?>
    <div class="linegrey"></div>
    <div class="linewhite"></div>
    <ul class="catal">
        <?=$more?>
    </ul>
<? } ?>