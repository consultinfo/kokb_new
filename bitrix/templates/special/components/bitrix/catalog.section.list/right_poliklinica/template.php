<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<ul class="polik">
    <?foreach($arResult["SECTIONS"] as $arSection):?>
    <?
	if ($arSection["NAME"] == getMessage("LONG_OTD")) {
		$arSection["NAME"] = getMessage("SHORT_OTD");
	}
	?>
        <li class="polik_<?=$arSection["ID"]?>"><div><a href="<?=$arSection["SECTION_PAGE_URL"]?>"><?=htmlspecialchars_decode($arSection["NAME"])?></a></div></li>
    <?endforeach?>
</ul>