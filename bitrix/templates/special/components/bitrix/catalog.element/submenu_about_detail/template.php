<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.css" />


<table border="0" cellpadding="0" cellspacing="0" class="detail_specialist">
    <?if ($arResult["PROPERTIES"]["GLAV_DOCTOR"]["VALUE"] != ""):?>
        <tr>
            <td colspan="2" class="glavvrach">
            	<?
                $arIBlockElement = GetIBlockElement($arResult["PROPERTIES"]["GLAV_DOCTOR"]["VALUE"], 'foundations');
				$arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
				$imgResize = CFile::ResizeImageGet($arImg, array("width" => 485, 'height' => 500), BX_RESIZE_IMAGE_PROPORTIONAL, true);				
				?>
                <img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" class="glav_fon" />
                
                <div class="glav_name"><?=$arIBlockElement["NAME"]?></div>
                <div class="glav_desc"><?=$arIBlockElement["PREVIEW_TEXT"]?></div>
			</td>
        </tr>
        <tr>
            <td colspan="2"><br /></td>
        </tr>
    <?endif?>
    
    <?if ($arResult["DETAIL_TEXT"] != ""):?>
        <tr>
            <td colspan="2"><?=$arResult["DETAIL_TEXT"]?></td>
        </tr>
        <tr>
            <td colspan="2"><br /></td>
        </tr>
    <?endif?>
    <? if (is_array($arResult["PROPERTIES"]["STAFF"]["VALUE"]) AND count($arResult["PROPERTIES"]["STAFF"]["VALUE"]) > 0):?>
		<? foreach($arResult["PROPERTIES"]["STAFF"]["VALUE"] as $val):
            $arIBlockElement = GetIBlockElement($val, 'foundations');
            $arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
			$imgResize = CFile::ResizeImageGet($arImg, array("width" => 150, 'height' => 225), BX_RESIZE_IMAGE_PROPORTIONAL, true);
        ?>
            <tr>
                <td align="left" valign="top" width="150" colspan="2">
                    <img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" class="img_fon" />

                	<?if ($arResult["PROPERTIES"]["GLAV_DOCTOR"]["VALUE"] != ""){?>
                    	<div class="name_blue"><?=$arIBlockElement["NAME"]?></div>
                    <? } else { ?>
                    	<div class="name"><?=$arIBlockElement["NAME"]?></div>
                    <? } ?>
					
                    <?
					$ID = trim($_GET["ID"]);
					?>
                    
                    <? if($ID == 411) { ?>
                    	<div class="desc"><?=$arIBlockElement["PROPERTIES"]["RUKOVOD"]["VALUE"]["TEXT"]?></div>
                    <? } elseif ($ID == 412) { ?>
                    	<div class="desc"><?=$arIBlockElement["PROPERTIES"]["GOOD_DOCTOR"]["VALUE"]["TEXT"]?></div>
                    <? } else { ?>
                    	<div class="desc"><?=$arIBlockElement["PREVIEW_TEXT"]?></div>
                    <? } ?>
                    
                    
                    <? /*if ($arIBlockElement["PROPERTIES"]["RUKOVOD"]["VALUE"]["TEXT"] != "") { ?>
                    	<div class="desc"><?=$arIBlockElement["PROPERTIES"]["RUKOVOD"]["VALUE"]["TEXT"]?></div>
                    <? } elseif ($arIBlockElement["PROPERTIES"]["GOOD_DOCTOR"]["VALUE"]["TEXT"] != "") { ?>
                    	<div class="desc"><?=$arIBlockElement["PROPERTIES"]["GOOD_DOCTOR"]["VALUE"]["TEXT"]?></div>
                    <? } else { ?>
                    	<div class="desc"><?=$arIBlockElement["PREVIEW_TEXT"]?></div>
                    <? } */?>
                </td>
            </tr>
            <tr><td colspan="2"><br /></td></tr>
        <?endforeach;?>
    <?endif?>
    <tr>
    	<td colspan="2">

        	<?if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["VALUE"]) != 0): ?>
            	<div id="gallery">
                    <ul>
                        <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO_CATAL"]["FILE_VALUE"] as $key => $val):?>
                                <?	
                                    $imgTmpBig = CFile::ResizeImageGet($val, array("width" => 600, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                                
                                    $imgTmpSmall = CFile::ResizeImageGet($val, array("width" => 100, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                                    if (strlen($val["DESCRIPTION"]) >= 254) {
                                        $val["DESCRIPTION"] .= "...";
                                    }
                                ?>
                                <li>
                                    <a href="<?=$imgTmpBig["src"]?>" title="<?=$val["DESCRIPTION"]?>">
                                        <img src="<?=$imgTmpSmall["src"]?>" alt="" />
                                    </a>
                                </li>
                        <?endforeach?>
                    </ul>
                </div>
            <?endif;?>
        </td>
    </tr>
</table>

<script type="text/javascript">
		$(function() {
			$('#gallery a').lightBox();
			$('#photo_detail a').lightBox();
		});
</script>