<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/litebox/jquery.lightbox-0.5.css" />


<?//$APPLICATION->SetTitle($arResult["SECTION"]["NAME"]);?>

<!-- photo glav -->
<?
$arIBlockElement = GetIBlockElement($arResult["PROPERTIES"]["SUPERVISION"]["VALUE"], 'foundations');
$arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
$arImgBig = CFile::GetFileArray($arIBlockElement["DETAIL_PICTURE"]);

$arImgBigResize = CFile::ResizeImageGet($arImgBig, array("width" => 600, 'height' => 450), BX_RESIZE_IMAGE_PROPORTIONAL, true);

?>
<?if(is_array($arImg)):?>
	<? $imgResize = CFile::ResizeImageGet($arImg, array("width" => 150, 'height' => 225), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
     <div id="photo_detail">
         <div class="fon">
         	<div class="img_center">
            <? if (isset($arImgBigResize) AND count($arImgBigResize) > 1) { ?>
             <a href="<?=$arImgBigResize["src"]?>" title="<?=$arIBlockElement["NAME"]?> - <?=$arIBlockElement["PREVIEW_TEXT"]?>"><img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" /></a>
            <? } else { ?>
            	<img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" />
            <? } ?>
            </div>
             <div class="fio"><?=$arIBlockElement["NAME"]?></div>
             <div class="short"><?=$arIBlockElement["PREVIEW_TEXT"]?></div>
         </div>
    </div>
<?endif;?>
<!-- description -->
<div id="text_detail">
    <?=$arResult["DETAIL_TEXT"]?>
    <?if ($arResult["PROPERTIES"]["OTDEL_PHONE"]["VALUE"] != "") {?>
	<div class="phone_otdel"><?=GetMessage("OTD_PHONE")?> <span><?=$arResult["PROPERTIES"]["OTDEL_PHONE"]["VALUE"]?></span></div>
<?}?>
</div>
<div class="br"></div><br />

<!-- sotrudnik list -->
<? if (is_array($arResult["PROPERTIES"]["STAFF"]["VALUE"]) AND count($arResult["PROPERTIES"]["STAFF"]["VALUE"]) > 0) { ?>
	<? $rez = ""; ?>
	<? foreach($arResult["PROPERTIES"]["STAFF"]["VALUE"] as $val):
			$arIBlockElement = GetIBlockElement($val, 'foundations');
			$arImg = CFile::GetFileArray($arIBlockElement["PREVIEW_PICTURE"]);
			$imgResize = CFile::ResizeImageGet($arImg, array("width" => 150, 'height' => 225), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			if ($arIBlockElement["DETAIL_TEXT"] != "") {
	?>
                <img src="<?=$imgResize["src"]?>" alt="<?=$arIBlockElement["NAME"]?>" title="<?=$arIBlockElement["NAME"]?>" class="img_fon_big" />
                <span class="name"><?=$arIBlockElement["NAME"]?></span><br />
                <?=$arIBlockElement["DETAIL_TEXT"]?>
                <div class="br"></div><br />
            <? }
			els