<?
$MESS ['MFT_NAME'] = "Name";
$MESS ['MFT_EMAIL'] = "Your E-mail";
$MESS ['MFT_PHONE'] = "Phone";
$MESS ['MFT_ADRES'] = "Adres";
$MESS ['MFT_MESSAGE'] = "Message";
$MESS ['MFT_TOPIC'] = "Topic";
$MESS ['MFT_ORG'] = "Organization";
$MESS ['MFT_FILE'] = "File";
$MESS ['MFT_CAPTCHA'] = "CAPTCHA";
$MESS ['MFT_CAPTCHA_CODE'] = "Type the letters you see on the picture";
$MESS ['MFT_SUBMIT'] = "Send";
?>