<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

				<div id="main_news">
                	<div class="news_text">
                    	<h2><?=GetMessage("TMP_NEWS")?></h2>
                       	<table border="0" cellpadding="0" cellspacing="0" class="news">
<?foreach($arResult["ITEMS"] as $arItem):?>
                           	<tr>
                               <td class="img">
                               	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
                                    <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
                                        <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" /></a>
                                    <?else:?>
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>" height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" />
                                    <?endif;?>
                                <?endif?>
                               </td>
                               <td>
                                  <?if($arParams["DISPLAY_DATE"]!="N" && $arItem["DISPLAY_ACTIVE_FROM"]):?>
                                     <div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
                                  <?endif?>
                                  <div class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></div>
                                  <div class="short"><?=$arItem["PREVIEW_TEXT"];?></div>
                               </td>
                           </tr>
<?endforeach;?>

                           <tr>
                           		<td></td>
                                <td><a href="/about/news/" class="news_all"><span><?=GetMessage("ALL_NEWS")?></span></a></td>
                           </tr>
                        </table>
                    </div><!-- end .news_text -->
                </div><!-- end #main_news -->
                <div class="line_main"></div>