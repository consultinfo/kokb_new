$(document).ready(function($) {

	$('select').on('change', '.select select', function (e) {

		var filter = $(this.value);
		var selind = document.getElementById("select").options.selectedIndex;
		var val= document.getElementById("select").options[selind].value;
		var text= document.getElementById("select").options[selind].text;

		var selind = document.getElementById("year").options.selectedIndex;
		var year = document.getElementById("year").options[selind].value;
		if (typeof ar_title !== "undefined") {
			var title = $("#title");
			title.text(text);
		}

		$.ajax({
			url: page,
			method: "post",
			data: {
				AJAX: "Y",
				FILTER: val,
				YEAR: year
			},
			beforeSend: function () {
				// $('#wrp_content').fadeOut(200);
			},
			error: function () {
				alert("Ошибка запроса");
			},
			success: function (result) {
				// $('#wrp_content').html(result).fadeIn(200);
				// $("select").styler();
				if (val == "") {
					window.history.pushState(null, null, "?YEAR=" + year);
				}
				if (year == "") {
					window.history.pushState(null, null, "?FILTER=" + val);
				}
				if (year == "" && val == "") {
					window.history.pushState(null, null, page);
				}
				if(val != "" && year != ""){
					window.history.pushState(null, null, "?FILTER=" + val + "&YEAR=" + year);
				}
			}
		})

	});
})