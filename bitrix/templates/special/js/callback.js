/***********************************
	CallBack Functions
	------------------

	List:
	-------------------------

	--- document.ready ---
	
	[0] - gallery carousel [owl.carousel]

	--- document.scroll ---

	[0] - Show/hide logotype in nav.navigation

	--- document.resize ---

	[0] - remove attr 'display:none' on resize

	--- document.mouseup ---

	[0] -



 ***********************************/

/****** DOCUMENT READY ******/

$(document).ready(function(){

	/**** CONSTANTS ****/

	var top_navigation = $('nav.navigation ul.navi'),
		main_carousel = $("#main"),
		search_trgr = $('#navigation .search_wrp .search_button a'),
		questions_wrp = $('#faq .questions_wrp .question_list'),
		questions_trgr = $('#faq .show_more a'),
		sidebar_select = $('.select select'),
		message = $('#message');


	/**** /CONSTANTS ****/

	/**** [0] ****/

	if (main_carousel.size()){
		main_carousel.owlCarousel({
			autoplay: true,
			autoplayTimeout: 5000,
			smartSpeed: 1000,
			items: 1,
			loop: true,
			nav: true,
			navText: ["",""],
			dots: true,
			autoplayHoverPause: true,
			responsiveClass: true
		});
	}

	/**** [1] ****/

	if( top_navigation.size() ){
		top_navigation.slimmenu({
			resizeWidth: '1199',
			collapserTitle: '',
			easingEffect:'easeInOutQuint',
			animSpeed:'medium',
			indentChildren: true,
			childrenIndenter: ''
		});

		var resolution = "1199";
		navigation(resolution);

	}

	/**** [2] ****/

	var news = $('#news .items_wrap'),
		news_resolution = 481,
		news_dots = true,
		news_nav = false,
		news_qnt = 1;

	enable_carousel(news, news_resolution, news_dots, news_nav, news_qnt);

	var state_banners = $('#state_banners .items_wrap'),
		state_banners_resolution = 991,
		state_banners_dots = true,
		state_banners_nav = false,
		state_banners_qnt = 2;

	enable_carousel(state_banners, state_banners_resolution, state_banners_dots, state_banners_nav, state_banners_qnt);

	/**** [3] ****/

	search_trgr.click(function(){
		search_bar.toggleClass('expanded');
	});

	/**** [4] ****/

	var animateTime = 600,
		current_height = questions_wrp.height();

	questions_trgr.click(function(){

		if( questions_wrp.height() === current_height ){
			show_questions(questions_wrp, animateTime);
			var link_txt = "Свернуть список";
			questions_trgr.html(link_txt);

		} else {
			questions_wrp.stop().animate({ height: current_height }, animateTime);
			var link_txt = questions_trgr.data('label');
			questions_trgr.html(link_txt);
		}
	});


	/*------[5]------*/

	$(".back2top a").click(function(evn){
		evn.preventDefault();
		$('html,body').scrollTo(this.hash, this.hash);
	});

	/*------[6]------*/

	if (sidebar_select.size())
	{
		sidebar_select.styler();
	}

	/*------[7]------*/

	var app_trigger = $('.app_type_wrp .item input');

	app_trigger.change(function() {
		$('.app_type_container').removeClass('active');
		$('#' + $(this).data('id')).addClass('active');

	});

	/*------[8]------*/

	var sub_navigation = $("nav.sub_navigation ul");
	if (sub_navigation.size())
	{
		sub_navigation.menutron({
			maxScreenWidth: 991
		});
	}
	var sub_navigation_select = $('nav.sub_navigation select')
	if (sub_navigation_select.size())
	{
		sub_navigation_select.styler();
	}

	/*------[9]------*/

	if (message.size()){
		message.owlCarousel({
			autoplay: false,
			autoplayTimeout: 5000,
			smartSpeed: 1000,
			items: 1,
			loop: true,
			nav: true,
			navText: ["",""],
			dots: true,
			autoplayHoverPause: true,
			responsiveClass: true
		});
	}

	/*------[10]------*/

	social_networks();

});

/****** WINDOW RESIZE ******/

$(window).resize(function(){

	/**** [0] ****/

	var news = $('#news .items_wrap'),
		news_resolution = 481,
		news_dots = true,
		news_nav = false,
		news_qnt = 1;

	enable_carousel(news, news_resolution, news_dots, news_nav, news_qnt);

	var state_banners = $('#state_banners .items_wrap'),
		state_banners_resolution = 991,
		state_banners_dots = true,
		state_banners_nav = false,
		state_banners_qnt = 2;

	enable_carousel(state_banners, state_banners_resolution, state_banners_dots, state_banners_nav, state_banners_qnt);

	/**** [1] ****/

	social_networks();

});


/****** DOCUMENT SCROLL ******/

$(document).scroll(function(){

	/**** [0] ****/

	nav_bar = $('#navigation');

	if($(window).scrollTop() > 199){
		nav_bar.addClass( 'sticked' );
	} else{
		nav_bar.removeClass( 'sticked' );
	}

});


/****** DOCUMENT MOUSEUP ******/

$(document).mouseup(function(e){

	/**** [0] ****/
	search_bar = $('#navigation .search_wrp .search_bar');

	if (!search_bar.is(e.target) && search_bar.has(e.target).length === 0) {
		search_bar.removeClass('expanded');
	}

});


/****** FUNCTIONS ******/

//Enable owl.carousel
function enable_carousel(id, resolution, dots, nav, qnt) {

	if ( $(window).width() <= resolution ) {

		if ( id.size() ) {
			$(this).addClass('owl-carousel');
			id.owlCarousel({
				items: 1,
				loop: false,
				nav: nav,
				dots: dots,
				navText: [ '' ,'' ],
				responsiveClass: true,
				autoHeight:true,
				responsive:{
					0:{
						items:1
					},
					321:{
						items:1
					},
					481:{
						items:1
					},
					600:{
						items:1
					},
					767:{
						items:qnt
					}
				}
			});
		}

	}
	else if (id.hasClass('owl-carousel')) {
		id.data('owlCarousel').destroy();
		id.removeClass('owl-carousel owl-responsive--1 owl-loaded');
		id.find('.item').appendTo(id);
		id.find('.owl-stage-outer').remove();
	}

}

//top navigation
function navigation(resolution) {

	screen_width = $(window).width(),
		lvl_st = $('nav.navigation ul.navi li'),
		lvl_nd = $('.lvl_nd');
		lvl_rd = $('.lvl_rd');

	if ( window.screen_width >= resolution ) {

		lvl_st.on('mouseenter mouseleave', function(){
			$(this).find('a').toggleClass('selected');
			$(this).find(lvl_nd).stop().slideToggle(400);

		});

	} else if ( window.screen_width <= resolution ) {
		lvl_st.off('mouseenter mouseleave');
		lvl_st.find('span.trigger_nd').on('click', function(){
			$(this).toggleClass('active');
			$(this).next().slideToggle(400);
		});

		$('.has_rd').find('span.trigger_rd').on('click', function(){
			$(this).toggleClass('active');
			$(this).next().slideToggle(400);
		});

	}

	$(window).resize(function(){

		$('nav.navigation .lvl_nd').css('display', '');
		$('nav.navigation ul.navi li.has_nd').find('span.trigger_nd').removeClass('active');
		$('nav.navigation .lvl_rd').css('display', '');
		$('nav.navigation .elm.has_rd').find('span.trigger_rd').removeClass('active');

	});


}

//Show/hide questions
function show_questions(element, time){
	var current_height = element.height(),
		autoHeight = element.css('height', 'auto').height();

	element.height(current_height);
	element.stop().animate({ height: autoHeight }, parseInt(time));
}

function social_networks() {

	if ( $(window).width() <= 599 &&  !$('#navigation .social_networks').length ) {

		var social = $('.logotype_area_wrp .controls_wrp .social_networks');

		social.appendTo('#navigation > .line');

	} else if ( $(window).width() >= 600 && !$('.logotype_area_wrp .controls_wrp .social_networks').length ) {

		var social = $($('#navigation .line .social_networks')[0]);

		social.appendTo('.logotype_area_wrp .controls_wrp');
		$('#navigation .line .social_networks').remove();
	}

	return true;

}



/****** /FUNCTIONS ******/