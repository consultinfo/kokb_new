<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//echo '<pre>'; print_r($arResult); echo '</pre>';

if (empty($arResult))
	return;
?>

<div class="leftMenu">
	<div class="clearfix">
		<ul>
		<?foreach($arResult as $itemIdex => $arItem)
		{
			if($arItem["LINK"]=="/company/personal/") continue;
			if ($arItem["PERMISSION"] > "D" && $arItem["DEPTH_LEVEL"] == 1){?>
		
			<li <?if($arItem["SELECTED"]):?> class="bord current"<?endif?>>
				<?if ($arItem["SELECTED"]):?>
					<span><span><?=$arItem["TEXT"]?></span></span>
				<?else:?>
					<a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
				<?endif?>
				<em></em>
			</li>
			<? } else {?>
				<li class="child <?if($arItem["SELECTED"]):?>current<?endif?>">
					<?if ($arItem["SELECTED"]):?>
						<span><span><?=$arItem["TEXT"]?></span></span>
					<?else:?>
						<a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
					<?endif?>
					<em></em>
				</li>
		<?	}
		}?>
		
		</ul>
	</div>
</div>


