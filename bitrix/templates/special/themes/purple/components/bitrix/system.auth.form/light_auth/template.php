<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["FORM_TYPE"] == "login"):?>

<div id="login-form-window">
<div id="login-form-window-header">
<div onclick="return authFormWindow.CloseLoginForm()" id="close-form-window" title="<?=GetMessage('AUTH_CLOSE_WINDOW_TITLE')?>"><?=GetMessage('AUTH_CLOSE_WINDOW')?></div><b><?=GetMessage('AUTH_')?></b>
</div>
<form method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
	<?if (strlen($arResult["BACKURL"]) > 0):?>
		<input type='hidden' name='backurl' value='<?=$arResult["BACKURL"]?>' />
	<?endif?>
	<?foreach ($arResult["POST"] as $key => $value):?>
		<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
	<?endforeach?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="AUTH" />
	<table align="center" cellspacing="0" cellpadding="4">
		<tr>
			<td align="right" width="30%"><?=GetMessage("AUTH_LOGIN")?>:</td>
			<td><input type="text" name="USER_LOGIN" id="auth-user-login" maxlength="50" value="<?=$arResult["USER_LOGIN"]?>" size="12" tabindex="1" /></td>
		</tr>
		<tr>
			<td align="right"><?=GetMessage("AUTH_PASSWORD")?>:</td>
			<td><input type="password" name="USER_PASSWORD" maxlength="50" size="12" tabindex="2" /><br /></td>
		</tr>
		<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
		<tr>
			<td></td>
			<td><input type="checkbox" id="USER_REMEMBER" name="USER_REMEMBER" value="Y" tabindex="3" checked="checked" /><label class="remember-text" for="USER_REMEMBER"><?=GetMessage("AUTH_REMEMBER_ME")?></label></td>
		</tr>
		<?endif?>
		<tr>
			<td></td>
			<td>
				<input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" tabindex="4" /><br />
				<a href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>"><?=GetMessage("AUTH_FORGOT_PASSWORD_2")?></a><br />
				<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
					<a href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTER")?></a><br />
				<?endif?>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
		</tr>
	</table>
</form>
</div>

<?if ($arResult["SHOW_ERRORS"] == "Y" && $arResult["ERROR"] === true):?>
	<span class="errortext"><?=(is_array($arResult["ERROR_MESSAGE"]) ? $arResult["ERROR_MESSAGE"]["MESSAGE"] : $arResult["ERROR_MESSAGE"])?></span>
<?endif?>
<div class="exit">
	<p><a href="/auth/" onclick="return authFormWindow.ShowLoginForm()"><?=GetMessage("AUTH_LOGIN_BUTTON")?></a></p>
	<?/*if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
	<span>&nbsp;|&nbsp;</span>
	<a href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTER")?></a>
	<?endif*/?>
</div>

<?else:

	$isNTLM = false;
	if (COption::GetOptionString("ldap", "use_ntlm", "N") == "Y")
	{
		$ntlm_varname = trim(COption::GetOptionString("ldap", "ntlm_varname", "REMOTE_USER"));
		if (array_key_exists($ntlm_varname, $_SERVER) && strlen($_SERVER[$ntlm_varname]) > 0)
			$isNTLM = true;
	}

	$params = DeleteParam(array("logout", "login", "back_url_pub"));
	$logoutUrl = $APPLICATION->GetCurPage()."?logout=yes".htmlspecialchars($params == ""? "":"&".$params);
?>

	<div class="exit"><?=GetMessage("AUTH_WELCOME_TEXT")?> <?=(strlen($arResult["USER_NAME"]) > 0 ? $arResult["USER_NAME"] : $arResult["USER_LOGIN"])?><?if ($isNTLM === false):?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?=$logoutUrl?>"><?=GetMessage("AUTH_LOGOUT_BUTTON")?></a><?endif?></div>
<?endif?>