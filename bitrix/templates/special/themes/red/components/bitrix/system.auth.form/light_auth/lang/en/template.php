<?
$MESS ['AUTH_LOGIN_BUTTON'] = "Login";
$MESS ["AUTH_CLOSE_WINDOW"] = "Close";
$MESS ['AUTH_LOGOUT_BUTTON'] = "Logout";
$MESS ['AUTH_REMEMBER_ME'] = "Remember me";
$MESS ['AUTH_FORGOT_PASSWORD_2'] = "Forgot your password?";
$MESS ['AUTH_REGISTER'] = "Register";
$MESS ["AUTH_LOGIN"] = "Login";
$MESS ["AUTH_PASSWORD"] = "Password";
$MESS ["AUTH_PROFILE"] = "My profile";
$MESS ["AUTH_MP"] = "My portal";
?>