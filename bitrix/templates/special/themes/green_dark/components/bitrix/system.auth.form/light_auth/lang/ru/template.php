<?
$MESS ['AUTH_LOGIN_BUTTON'] = 'Войти';
$MESS ['AUTH_CLOSE_WINDOW'] = 'Закрыть';
$MESS ['AUTH_CLOSE_WINDOW_TITLE'] = 'Закрыть окно';
$MESS ['AUTH_LOGIN'] = 'Логин';
$MESS ['AUTH_'] = 'Авторизация';
$MESS ['AUTH_PASSWORD'] = 'Пароль';
$MESS ['AUTH_REMEMBER_ME'] = 'Запомнить меня';
$MESS ['AUTH_FORGOT_PASSWORD_2'] = 'Забыли пароль?';
$MESS ['AUTH_REGISTER'] = 'Регистрация';
$MESS ['AUTH_LOGOUT_BUTTON'] = 'Выйти';
$MESS ['AUTH_PROFILE'] = 'Изменить профиль';
$MESS ['AUTH_PERSONAL_PAGE'] = 'Личная страница';
$MESS ['AUTH_NEW_MESSAGES'] = 'Мои сообщения';
$MESS ['AUTH_MP'] = 'Мой портал';
$MESS ['AUTH_WELCOME_TEXT'] = 'Здравствуйте,';
$MESS ['AUTH_CABINET_BUTTON'] = 'Мой кабинет';
?>