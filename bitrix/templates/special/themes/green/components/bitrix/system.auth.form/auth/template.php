<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["FORM_TYPE"] == "login"):
	?>
	<div id="user-block">
		<div id="user-block-wrapper">
			<?
			if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'])
			{
				ShowMessage($arResult['ERROR_MESSAGE']);
			}
			?>		
			<form method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
			<?if (strlen($arResult["BACKURL"]) > 0):?>
				<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
			<?endif?>
			<?foreach ($arResult["POST"] as $key => $value):?>
				<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
			<?endforeach?>
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<table cellspacing="0" id="auth-form">
				<tr>
					<td align="right" colspan="2">
					<?if($arResult["NEW_USER_REGISTRATION"] == "Y"):?>
						<a href="<?=$arResult["AUTH_REGISTER_URL"]?>"><?=GetMessage("AUTH_REGISTER")?></a>&nbsp;&nbsp;&nbsp;
					<?endif?>
					<a title="<?=GetMessage("AUTH_FORGOT_PASSWORD_2")?>" href="<?=$arResult["AUTH_FORGOT_PASSWORD_URL"]?>">?</a></td>
				</tr>
				<tr>
					<td class="field-name"><label for="login-textbox"><?=GetMessage("AUTH_LOGIN")?>:</label></td>
					<td><input type="text" name="USER_LOGIN" id="login-textbox" class="textbox" value="<?=$arResult["USER_LOGIN"]?>" tabindex="1" /></td>
				</tr>
				<tr>
					<td class="field-name"><label for="password-textbox"><?=GetMessage("AUTH_PASSWORD")?>:</label></td>
					<td><input type="password" name="USER_PASSWORD" id="password-textbox" class="textbox" tabindex="2" /></td>
				</tr>
				<?if ($arResult["STORE_PASSWORD"] == "Y"):?>
				<tr>
					<td>&nbsp;</td>
					<td><input type="checkbox" name="USER_REMEMBER" class="checkbox" id="remember-checkbox" value="Y" tabindex="4" checked="checked" /><label class="remember" for="remember-checkbox"><?=GetMessage("AUTH_REMEMBER_ME")?></label></td>
				</tr>
				<?endif?>
				<tr>
					<td>&nbsp;</td>
					<td><input type="submit" name="Login" value="<?=GetMessage("AUTH_LOGIN_BUTTON")?>" tabindex="5" /></td>
				</tr>
			</table>
			</form>
		</div>
		<div id="user-block-corners">
			<div class="border"></div>
			<div class="corner left"></div>
			<div class="corner right"></div>
		</div>		
	</div>
<?else:

	$isNTLM = false;
	if (COption::GetOptionString("ldap", "use_ntlm", "N") == "Y")
	{
		$ntlm_varname = trim(COption::GetOptionString("ldap", "ntlm_varname", "REMOTE_USER"));
		if (array_key_exists($ntlm_varname, $_SERVER) && strlen($_SERVER[$ntlm_varname]) > 0)
			$isNTLM = true;
	}

	$params = DeleteParam(array("logout", "login", "back_url_pub"));
	$logoutUrl = $APPLICATION->GetCurPage()."?logout=yes".htmlspecialchars($params == ""? "":"&".$params);

	?>
	<div id="user-block">
		<div id="user-block-wrapper">

			<div id="user-info">
				<a id="user-info-avatar" <?if (strlen($arResult["urlToOwnProfile"]) > 0):?>href="<?=$arResult["urlToOwnProfile"]?>"<?endif;?> <?if (strlen($arResult["USER_PERSONAL_PHOTO_SRC"]) > 0):?>style="background-image: url(<?=$arResult["USER_PERSONAL_PHOTO_SRC"]?>); background-repeat: no-repeat; background-position: center center;"<?endif;?>></a>
				<span id="user-name"><?=$arResult["USER_NAME_FORMATTED"]?></span>
				<? if (strlen($arResult["urlToMyPortal"]) > 0):
					?><a id="user-desktop" href="<?=$arResult["urlToMyPortal"]?>"><?=GetMessage("AUTH_MP")?></a><? 
				endif;?>
				<?if ($isNTLM === false):?><a id="user-logout" href="<?=$logoutUrl?>"><?=GetMessage("AUTH_LOGOUT")?></a><?endif?>
			</div>

			<table id="user-menu" cellspacing="0">
				<tr>
					<td class="left-column">
						<ul>
							<? if (strlen($arResult["urlToOwnProfile"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnProfile"]?>"><?=GetMessage("AUTH_PERSONAL_PAGE")?></a></li><?
							endif;?>
							<? if (strlen($arResult["urlToOwnMessages"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnMessages"]?>"><?=GetMessage("AUTH_NEW_MESSAGES")?></a><?$APPLICATION->IncludeComponent("bitrix:socialnetwork.events_dyn", "popup", Array(
											"PATH_TO_USER"				=>	$arParams["PATH_TO_SONET_PROFILE"],
											"PATH_TO_GROUP"				=>	$arParams["PATH_TO_SONET_GROUP"],
											"PATH_TO_MESSAGES"			=>	$arParams["PATH_TO_SONET_MESSAGES"],
											"PATH_TO_MESSAGE_FORM"		=>	$arParams["PATH_TO_SONET_MESSAGE_FORM"],
											"PATH_TO_MESSAGE_FORM_MESS"	=>	$arParams["PATH_TO_SONET_MESSAGE_FORM_MESS"],
											"PATH_TO_MESSAGES_CHAT"	=>	$arParams["PATH_TO_SONET_MESSAGES_CHAT"],
											"PATH_TO_SMILE"	=>	"/bitrix/images/socialnetwork/smile/",
											"MESSAGE_VAR"	=>	"message_id",
											"PAGE_VAR"	=>	"page",
											"USER_VAR"	=>	"user_id",
											"POPUP"	=>	"Y",
										),
										false,
										array("HIDE_ICONS" => "Y")
									);
								?></li><? 
							endif;?>
							<? if (strlen($arResult["urlToOwnCalendar"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnCalendar"]?>"><?=GetMessage("AUTH_CALENDAR")?></a></li><?
							endif;?>
							<? if (strlen($arResult["urlToOwnTasks"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnTasks"]?>"><?=GetMessage("AUTH_TASKS")?></a></li><?
							endif;?>
							<? if (strlen($arResult["urlToOwnPhoto"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnPhoto"]?>"><?=GetMessage("AUTH_PHOTO")?></a></li><?
							endif;?>
						</ul>
					</td>
					<td class="center-column"></td>
					<td class="right-column">
						<ul>
							<? if (strlen($arResult["urlToOwnBlog"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnBlog"]?>"><?=GetMessage("AUTH_BLOG")?></a></li><?
							endif;?>
							<? if (strlen($arResult["urlToOwnFiles"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnFiles"]?>"><?=GetMessage("AUTH_FILES")?></a></li><?
							endif;?>
							<? if (strlen($arResult["urlToOwnGroups"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnGroups"]?>"><?=GetMessage("AUTH_GROUPS")?></a></li><?
							endif;?>
							<?
							if(strlen($arResult["urlToOwnBizProc"]) > 0 && array_key_exists("SHOW_BIZPROC", $arResult) && $arResult["SHOW_BIZPROC"])
							{
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnBizProc"]?>"><?=GetMessage("AUTH_BZP")?></a><?if (intval($arResult["BZP_CNT"]) > 0):?><i><?=$arResult["BZP_CNT"]?></i><?endif;?></li><?
							}
							?>
							<? if (strlen($arResult["urlToOwnLog"]) > 0):
								?><li><span class="item-dot"></span><a href="<?=$arResult["urlToOwnLog"]?>"><?=GetMessage("AUTH_LOG")?></a></li><? 
							endif;?>
						</ul>
					</td>
				</tr>
			</table>
		</div>
		<div id="user-block-corners">
			<div class="border"></div>
			<div class="corner left"></div>
			<div class="corner right"></div>
		</div>
	</div>
<?endif?>