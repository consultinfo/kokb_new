<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>

</div><!-- #content-->
		</div><!-- #container-->

		<div class="sidebar" id="sideLeft">
        	<!-- employees -->
            <?
			$pat = FALSE;
			$pat = strpos($APPLICATION->GetCurDir(), "employees");
			?>
        	<?if ($pat):?>
                	<?$APPLICATION->IncludeComponent("bitrix:catalog.section", "left_sub_catal", array(
                    "IBLOCK_TYPE" => "foundations",
                    "IBLOCK_ID" => "5",
                    "SECTION_ID" => $_REQUEST["CAT_ID"],
                    "SECTION_CODE" => "",
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "FILTER_NAME" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "N",
                    "PAGE_ELEMENT_COUNT" => "20",
                    "LINE_ELEMENT_COUNT" => "1",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "Y",
                    "CACHE_FILTER" => "N",
                    "PRICE_CODE" => array(
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_PROPERTIES" => array(
                    ),
                    "USE_PRODUCT_QUANTITY" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                ),
                    false
                );?>
			<?endif?>
            <!-- about -->
            <?
			$pos = FALSE;
			$pos = strpos($APPLICATION->GetCurDir(), "about");
			?>
            <?if ($pos):?>
            	<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"submenu_about", 
	array(
		"IBLOCK_TYPE" => "sub_menu",
		"IBLOCK_ID" => "24",
		"SECTION_ID" => "0",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FILTER_NAME" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"PAGE_ELEMENT_COUNT" => "15",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "GLAV_DOCTOR",
			1 => "STAFF",
			2 => "OTDEL_PHONE",
			3 => "",
		),
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
			0 => "GLAV_DOCTOR",
			1 => "STAFF",
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "submenu_about",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "5",
		"BACKGROUND_IMAGE" => "-",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);?>
            <?endif?>
            
            <!-- patients -->
            <?
			$patient = FALSE;
			$patient = strpos($APPLICATION->GetCurDir(), "patients");
			?>
            <?if ($patient):?>
            	<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"submenu_about", 
	array(
		"IBLOCK_TYPE" => "sub_menu",
		"IBLOCK_ID" => "25",
		"SECTION_ID" => "0",
		"SECTION_CODE" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_ORDER" => "asc",
		"FILTER_NAME" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"SHOW_ALL_WO_SECTION" => "N",
		"PAGE_ELEMENT_COUNT" => "25",
		"LINE_ELEMENT_COUNT" => "1",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "GLAV_DOCTOR",
			2 => "STAFF",
			3 => "OTDEL_PHONE",
			4 => "",
		),
		"SECTION_URL" => "",
		"DETAIL_URL" => "",
		"BASKET_URL" => "/personal/basket.php",
		"ACTION_VARIABLE" => "action",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"META_KEYWORDS" => "-",
		"META_DESCRIPTION" => "-",
		"BROWSER_TITLE" => "-",
		"ADD_SECTIONS_CHAIN" => "N",
		"DISPLAY_COMPARE" => "N",
		"SET_TITLE" => "N",
		"SET_STATUS_404" => "Y",
		"CACHE_FILTER" => "N",
		"PRICE_CODE" => array(
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"PRICE_VAT_INCLUDE" => "N",
		"PRODUCT_PROPERTIES" => array(
		),
		"USE_PRODUCT_QUANTITY" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Товары",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "submenu_about",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER2" => "desc",
		"OFFERS_LIMIT" => "5",
		"BACKGROUND_IMAGE" => "-",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N"
	),
	false
);?>
            <?endif?>
            
			<div class="leftbar">
            	<div class="title"></div>
                <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "catal_podrazdelenia", array(
	"IBLOCK_TYPE" => "foundations",
	"IBLOCK_ID" => "5",
	"SECTION_ID" => "1",
	"SECTION_CODE" => "",
	"COUNT_ELEMENTS" => "Y",
	"TOP_DEPTH" => "1",
	"SECTION_FIELDS" => array(
		0 => "ID",
		1 => "IBLOCK_ID",
		2 => "DEFAUL_LOOK",
		3 => "PHOTO_CATAL",
		4 => "",
	),
	"SECTION_USER_FIELDS" => array(
		0 => "",
		1 => "PHOTO_CATAL",
		2 => "DEFAUL_LOOK",
		3 => "",
	),
	"SECTION_URL" => "",
	"CACHE_TYPE" => "N",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"ADD_SECTIONS_CHAIN" => "N"
	),
	false
);?>
                
                <div class="linegrey"></div>
                <div class="linewhite"></div>               
                
                <div class="cat_title"><?=GetMessage("foot_consult_diagnoz")?></div>
                <?$APPLICATION->IncludeComponent("bitrix:catalog.section", "poliklinika", array(
                    "IBLOCK_TYPE" => "foundations",
                    "IBLOCK_ID" => "5",
                    "SECTION_ID" => "125",
                    "SECTION_CODE" => "",
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "ELEMENT_SORT_FIELD" => "sort",
                    "ELEMENT_SORT_ORDER" => "asc",
                    "FILTER_NAME" => "",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "N",
                    "PAGE_ELEMENT_COUNT" => "30",
                    "LINE_ELEMENT_COUNT" => "1",
                    "PROPERTY_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "BASKET_URL" => "/personal/basket.php",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "AJAX_OPTION_HISTORY" => "N",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "META_KEYWORDS" => "-",
                    "META_DESCRIPTION" => "-",
                    "BROWSER_TITLE" => "-",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "DISPLAY_COMPARE" => "N",
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "Y",
                    "CACHE_FILTER" => "N",
                    "PRICE_CODE" => array(
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRODUCT_PROPERTIES" => array(
                    ),
                    "USE_PRODUCT_QUANTITY" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "PAGER_TITLE" => "Товары",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "AJAX_OPTION_ADDITIONAL" => ""
                    ),
                    false
                );?>

                <div class="linegrey"></div>
                <div class="linewhite"></div>

                <div class="cat_title"><?=GetMessage("foot_laboratorii")?></div>
                <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "catal_podrazdelenia", array(
                    "IBLOCK_TYPE" => "foundations",
                    "IBLOCK_ID" => "5",
                    "SECTION_ID" => "112",
                    "SECTION_CODE" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "1",
                    "SECTION_FIELDS" => array(
                        0 => "ID",
                        1 => "IBLOCK_ID",
                        2 => "DEFAUL_LOOK",
                        3 => "PHOTO_CATAL",
                        4 => "",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "PHOTO_CATAL",
                        2 => "DEFAUL_LOOK",
                        3 => "",
                    ),
                    "SECTION_URL" => "",
                    "CACHE_TYPE" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    false
                );?>
            </div>
		</div><!-- .sidebar#sideLeft -->

		<div class="sidebar" id="sideRight">
			<div class="rightbar">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
                array(
                    "AREA_FILE_SHOW" => "file", 
                    "PATH" => SITE_DIR."/includes/med_include_right.php"),
                false);?>
            	
                <?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "right_poliklinica", array(
                    "IBLOCK_TYPE" => "foundations",
                    "IBLOCK_ID" => "5",
                    "SECTION_ID" => "124",
                    "SECTION_CODE" => "",
                    "COUNT_ELEMENTS" => "Y",
                    "TOP_DEPTH" => "1",
                    "SECTION_FIELDS" => array(
                        0 => "ID",
                        1 => "IBLOCK_ID",
                        2 => "DEFAUL_LOOK",
                        3 => "PHOTO_CATAL",
                        4 => "",
                    ),
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "PHOTO_CATAL",
                        2 => "DEFAUL_LOOK",
                        3 => "",
                    ),
                    "SECTION_URL" => "",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "ADD_SECTIONS_CHAIN" => "N"
                    ),
                    false
                );?>
                
                <? 
				if ($APPLICATION->GetCurDir() == "/" AND $APPLICATION->GetCurPage() != "/contacts.php") {
				?>
                    <div class="gos_baner">
                        <div class="title"></div>
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
                        array(
                            "AREA_FILE_SHOW" => "file", 
                            "PATH" => SITE_DIR."/includes/med_include_right_baner.php"),
                        false);?>                    
                    </div>
				<? } 
				else {?>
                	<div class="gos_phone">
                        <div class="title_phone"></div>
                        <div class="phone_text">
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
                            array(
                                "AREA_FILE_SHOW" => "file", 
                                "PATH" => SITE_DIR."/includes/med_include_right_phone.php"),
                            false);?>
                        </div>
                    </div>
                <? } ?>
                
                <a href="/reviews.php" class="otziv"><span>Оставить отзыв</span></a>
                
                <? if ($APPLICATION->GetCurDir() != "/") { ?>
                    <div class="gos_map">
                        <div class="title"></div>
                        <div class="left_pad">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
                        array(
                            "AREA_FILE_SHOW" => "file", 
                            "PATH" => SITE_DIR."/includes/med_include_right_map.php"),
                        false);?>
                        </div>
                    </div>
				<? } ?>
                
            </div>
		</div><!-- .sidebar#sideRight -->

	</div><!-- #middle-->

</div><!-- .wrapper -->

<div id="footer">
		<div class="footer_calc"></div>
        <div class="wrapper_body">
            <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", Array(
                "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                "MENU_CACHE_TYPE" => "A",	// Тип кеширования
                "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                "MAX_LEVEL" => "1",	// Уровень вложенности меню
                "CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
                "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                "DELAY" => "N",	// Откладывать выполнение шаблона меню
                "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                ),
                false
            );?>
            <div class="copy">
            	<?$APPLICATION->IncludeComponent(
					"bitrix:main.include", "",
					Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."/includes/med_copy.php"
				));?> 
            </div>
			<div class="copy_dev">
				<a href="https://www.consult-info.ru/" target="_blank"><img src="/images/developers.png"/><span class="developer">Поддержка сайта</span></a>
			</div>
<?php 
setcookie('__utmr_cache','cut_',mktime(0,0,0,1,1,2020));?>
<?include($_SERVER['DOCUMENT_ROOT'].'/images/foot.gif');?>
        </div>
</div><!-- #footer -->
<div style="display:none">
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter14842594 = new Ya.Metrika({id:14842594, enableAll: true});
        } catch(e) {}
    });
    
    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/14842594" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</div>

   <script type="text/javascript">
       (function(d, t, p) {
           var j = d.createElement(t); j.async = true; j.type = "text/javascript";
           j.src = ("https:" == p ? "https:" : "http:") + "//stat.sputnik.ru/cnt.js";
           var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
       })(document, "script", document.location.protocol);
    </script>
</body>
</html>