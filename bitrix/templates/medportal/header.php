<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?IncludeTemplateLangFile(__FILE__);?>
<?
CUtil::InitJSCore();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
	<?$APPLICATION->ShowHead();?>
	<meta name="author" content="consult-info.ru"/>
	<meta name="yandex-verification" content="8b6aa5eabdd7e158" />
<meta name="cmsmagazine" content="ccacaf72b4dbd0a0e0be39f8c70d91ec" /> 
	<link rel="stylesheet" type="text/css" media="print" href="<?=SITE_TEMPLATE_PATH?>/print.css" />
	<link rel="alternate stylesheet" type="text/css" media="screen,projection" href="<?=SITE_TEMPLATE_PATH?>/print.css" title="print" />
	<title><?$APPLICATION->ShowTitle()?> - <?=COption::GetOptionString("sitemedicine", "site_personal_name")?></title>
    <meta name="yandex-verification" content="9351ea0dc237efb8" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.5.2.min.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/jsor-jcarousel/lib/jquery.jcarousel.min.js"></script>
	<?/*<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/jsor-jcarousel/skins/jquery.jcarousel.css" />*/?>
	<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/js/jsor-jcarousel/skins/tango/skin.css" />
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/script.js"></script>
</head>
<body>
	<?$APPLICATION->ShowPanel();?>
	<div class="wrapper">
	<div id="header">
		<a href="<?=SITE_DIR?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/head_med.png" alt="" class="med" /></a>
        <a href="<?=SITE_DIR?>"><img src="<?=SITE_TEMPLATE_PATH?>/images/head_name.png" alt="" class="name" /></a>
        <img src="<?=SITE_TEMPLATE_PATH?>/images/head_logo.png" alt="" class="logo" />
        <div class="contact">
        	<div class="phone">
            	<?$APPLICATION->IncludeComponent(
					"bitrix:main.include", "",
					Array(
						"AREA_FILE_SHOW" => "file",
						"PATH" => SITE_DIR."/includes/med_phone.php"
				));?> 
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array(
	"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."/includes/med_adres.php"
	),
	false,
	array(
	"ACTIVE_COMPONENT" => "N"
	)
);?> 
        </div>
<!--
        <?/*$APPLICATION->IncludeComponent("bitrix:system.auth.form", "light_auth", array(
					"REGISTER_URL" => "/auth/",
					"PROFILE_URL" => "/company/personal/user/".$USER->GetID()."/edit/",
					"SHOW_ERRORS" => "Y"
					),
					false
		);*/?>
        -->
        <div class="br"></div>
        
        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", array(
	"ROOT_MENU_TYPE" => "top",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "3",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "Y",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
	</div><!-- #header-->
</div>

<div class="wrapper_body">
	<div id="middle">

		<div id="container">
			<div id="content">
                <h1 id="pagetitle"><?$APPLICATION->ShowTitle();?></h1>