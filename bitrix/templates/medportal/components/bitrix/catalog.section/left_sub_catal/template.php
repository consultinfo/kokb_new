<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$EL_ID = IntVal($_GET["EL_ID"]);
$CAT_ID = IntVal($_GET["CAT_ID"]);
//print_r($arResult);
?>

<? if (isset($arResult["ITEMS"]) AND count($arResult["ITEMS"]) > 1): ?>
    <div class="leftbarsun">
    	<div class="main_bar_title">
        	<div class="bar_title" align="center"><div><?=$arResult["NAME"]?></div></div>
            <?$APPLICATION->SetTitle($arResult["NAME"]);?>
        </div>
        <ul class="subcatal">
        <?foreach($arResult["ITEMS"] as $cell => $arElement):?>
        	<?
			$active = "";
			if ($EL_ID == $arElement["ID"]) {
				$active = "class=\"active\"";
				$APPLICATION->SetTitle($arElement["NAME"]);
			}

            
            if ($arElement["ID"] == 545) {?>
				<li <?=$active?>><a href="/employees/inform/?CAT_ID=<?=$CAT_ID?>&EL_ID=<?=$arElement["ID"]?>"><?=$arElement["NAME"]?></a></li>
			<? } else { ?>
                <li <?=$active?>><a href="<?=$arElement["DETAIL_PAGE_URL"]?>"><?=$arElement["NAME"]?></a></li>
            <? } ?>
            
        <?endforeach?>
        </ul>
    </div>
<? endif?>