<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#",
		"RULE" => "alias=\$1",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/services/recomendations/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/services/recomendations/index.php",
	),
	array(
		"CONDITION" => "#^/services/price_list/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/services/price_list/index.php",
	),
	array(
		"CONDITION" => "#^/online/(/?)([^/]*)#",
		"RULE" => "",
		"ID" => "bitrix:im.router",
		"PATH" => "/desktop_app/router.php",
	),
	array(
		"CONDITION" => "#^/employees/inform/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/employees/inform/index.php",
	),
	array(
		"CONDITION" => "#^/patients/recom/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/patients/recom/index.php",
	),
	array(
		"CONDITION" => "#^/about/gallery/#",
		"RULE" => "",
		"ID" => "bitrix:photogallery",
		"PATH" => "/about/gallery/index.php",
	),
	array(
		"CONDITION" => "#^/about/advert/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/advert/index.php",
	),
	array(
		"CONDITION" => "#^/departments/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/departments/index.php",
	),
	array(
		"CONDITION" => "#^/about/jobs/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/jobs/index.php",
	),
	array(
		"CONDITION" => "#^/about/blog/#",
		"RULE" => "",
		"ID" => "bitrix:blog",
		"PATH" => "/about/blog/index.php",
	),
	array(
		"CONDITION" => "#^/about/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about/news/index.php",
	),
);

?>