<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>

    <main class="page-error">
        <div class="container">
            <p class="page-error__number">404</p>
            <p class="page-error__text">Страница не найдена</p>
        </div>
        </div>
    </main>
<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>